<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\Server;

class ServerController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $isActive = $this->checkUserStatus($session->get('id'));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login')); 
        }
        
        $session->set("page_id", "othersetting");
        $session->set("url", $this->generateUrl("admin_api_server"));

        if(isset($_POST['save'])){
            $model = $em->getRepository('AcmeAdminBundle:Server')->findOneBy(array('id'=>$_POST['id']));
            $model->setUrl($_POST['url']);
            $model->setPassGenApi($_POST['pass_gen_url']);
            $model->setFrontTransApi($_POST['front_trans_url']);
            $em->persist($model);
            $em->flush();
             
            $em->getConnection()->commit();

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated server URL to '.$_POST['url']);

            $this->get('session')->getFlashBag()->add(
                   'success',
                   $this->translateMessage('LBL_SERVER_UPDATED_SUCCESS').  $_POST['url']

               );
        }

        $api = $this->getAPIServer();
        
        return $this->render('AcmeAdminBundle:Server:index.html.twig',array('id'=>$api));

    }
}
