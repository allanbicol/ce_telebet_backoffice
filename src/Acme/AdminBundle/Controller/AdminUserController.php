<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\AdminUser;
use Acme\AdminBundle\Model\GlobalModel;

class AdminUserController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $data = $this->getAdminUser();
        
        $result = '';
        for($i=0; $i<count($data); $i++){
            $status_label = '';
            $delete = $this->translateMessage('LBL_USERS_DELETE');
            if ($data[$i]['status'] == 'deactivated'){
                $status_label = $this->translateMessage('LBL_USERS_ACTIVATE_TRANSLATION');
            }else{
                $status_label =$this->translateMessage('LBL_USERS_DEACTIVATE_TRANSLATION');
            }
            $init1 = $data[$i]['fname'];
            $init2 = $data[$i]['lname'];
            $result .= '
                      <tr>
                      <td><img class="img-circle profile_img" style="width: 50px; margin-top: -5px; border: none;" src="'.$this->generateUrl('admin_user_image',array('init'=>$init1[0].$init2[0],'id'=>$data[$i]['id'])).'"/></td>
                      <td>'.$data[$i]['email'].'</td>
                      <td>'.$data[$i]['fname'].'</td>
                      <td>'.$data[$i]['lname'].'</td>
                      <td>'.$data[$i]['status'].'</td>
                      <td>'.$data[$i]['last_datelogin'].'</td>
                      <td width="auto"><div style="width:290px;"><a href="'.$this->generateUrl('admin_user_edit',array('slug'=>$data[$i]['id'])).'"><button type="button" data-toggle="modal" class="btn btn-primary" data-target=".update-user">'.$this->translateMessage('LBL_USERS_EDIT_TRANSLATION').' </button></a> <a class="btn btn-warning" href="'.$this->generateUrl('admin_user_edit_status',array('slug'=>$data[$i]['id'])).'">'.$status_label.'</a><button data-id="'.$data[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>
                    </tr>';
        }
        
        $isActive = $this->checkUserStatus($session->get('id'));
        
        $session->set("page_id", "admin_user");
        $session->set("url", $this->generateUrl("admin_user"));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:AdminUser:index.html.twig',array('result'=>$result));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
         
    }
    
    public function addUserEditAction(){
        $mod = new GlobalModel;
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $session = $this->getRequest()->getSession();
        
        if($session->get('email') == ''){
            return new Response("session expired");
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
   
        $email = $_POST['email'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        
        $password = $mod->passGenerator($_POST['password']);
        
        $status = "active";
        if(isset($_POST['new'])){ 
            $a = $this->getAdminUserEmail($email);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_USERS_EMAIL_EXIST')
                );
                  return $this->redirect($this->generateUrl('admin_user_add')); 
              }
              else{
                $model = new AdminUser();
                $msg = $this->translateMessage('LBL_USERS_ADDED_SUCCESSFULLY');
              }
            //Insert Logs
            $this->insertLogs($session->get('id'),
                                $datetime->format("Y-m-d H:i:s"),
                                'Added new user with an email of '.$email);
        }else{
            $model = $em->getRepository('AcmeAdminBundle:AdminUser')->findOneBy(array('id'=>$_POST['id']));
            $msg = $this->translateMessage('LBL_USERS_UPDATED_SUCCESSFULLY');

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated the user with an email of '.$email);

        }
        $c1 = mt_rand(50,200); //r(ed)
        $c2 = mt_rand(50,200); //g(reen)
        $c3 = mt_rand(50,200); //b(lue)

        $bc = array('r'=>$c1,'g'=>$c2,'b'=>$c3);

        //$model = new AdminUser();
        $model->setFname($fname);
        $model->setLname($lname);
        $model->setEmail($email);
        if($_POST['password']!=''){
            $model->setPassword($password);
        }
        $model->setStatus($status);
        $model->setBackColor(json_encode($bc));
        
        $em->persist($model);
        $em->flush();
        $em->commit();
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
        return $this->redirect($this->generateUrl('admin_user')); 
    }
    
    public function updateUserAction($slug) {
        $session = $this->getRequest()->getSession();
         
        $data = $this->getAdminUserDetailsById($slug);
        $isActive = $this->checkUserStatus($session->get('id'));
        $session->set("page_id", "admin_user");
        $session->set("url", $this->generateUrl('admin_user_edit',array('slug'=>$slug)));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:AdminUser:admin_user_edit.html.twig',array('data'=>$data));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function updateUserStatusAction($slug) {

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('email') == ''){
            return new Response("session expired");
        }
        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
        
        $details = $this->getAdminUserDetailsById($slug);  
        if($details['status']=='active'){
            $status = 'deactivated';
            $msg = $this->translateMessage('LBL_USERS_USER_DEACTIVATED');
        }else{
            $status = 'active';
            $msg = $this->translateMessage('LBL_USERS_USER_ACTIVATED');
        }
        
        $model = $em->getRepository('AcmeAdminBundle:AdminUser')->findOneBy(array('id'=>$slug));
        
        $model->setStatus($status);
        
        $em->persist($model);
        $em->flush();
        $em->commit();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Updated the user status of'.$details['email']);

        $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
        return $this->redirect($this->generateUrl('admin_user')); 
    }
    
    public function addUserAction() {
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session->set("page_id", "admin_user");
        $session->set("url", $this->generateUrl("admin_user_add"));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:AdminUser:admin_user_add.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
    }

    public function generateUserImageAction($init,$id=null){
        $my_img = imagecreate( 140, 140 );
        $c1 = mt_rand(50,200); //r(ed)
        $c2 = mt_rand(50,200); //g(reen)
        $c3 = mt_rand(50,200); //b(lue)

        if($id==null){
            $background = imagecolorallocate( $my_img, $c1, $c2, $c3);
        }else{
            $user = $this->getAdminUserDetailsById($id);
            $img_back = json_decode($user['back_color'],true);

            $background = imagecolorallocate( $my_img, $img_back['r'], $img_back['g'], $img_back['b']);
        }

        $text_colour = imagecolorallocate( $my_img, 255, 255, 255 );
        $fontname = $this->get('kernel')->getRootDir() . '/../ap/font/Montserrat-Bold.ttf';

        imagettftext($my_img, 40, 0, 32, 85, $text_colour, $fontname,strtoupper($init));
        imagesetthickness ( $my_img, 5 );


        header( "Content-type: image/png" );
        imagepng( $my_img );

        imagecolordeallocate($text_colour);
        imagecolordeallocate($background);
        imagedestroy( $my_img );
    }
    
    public function deleteAdminUserAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();

        $details = $this->getAdminUserEmailByID($_POST['id']);
        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted user '.$details);
        
        $statement = $connection->prepare("DELETE FROM adminuser
            WHERE id=".$_POST['id']."");
        $statement->execute();

//        $this->get('session')->getFlashBag()->add(
//                'success',
//              $this->translateMessage('LBL_USERS_DELETE_SUCCESS')
//            );

        return $this->redirect($this->generateUrl('admin_user'));
    }
}
