<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;

class CardResultController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "SummaryResultPerTable");
        $session->set("url", $this->generateUrl("admin_detailed_result"));
        
        if(isset($_POST['btn_function'])){
            $date_from = $_POST['fltr_date_from'].' '.$_POST['fltr_hour_from'].':00:00';
            $date_to = $_POST['fltr_date_to'].' '.$_POST['fltr_hour_to'].':59:59';
            $results = $this->getDetailedResultPerTable($_POST['fltr_hall'],$_POST['fltr_table'],$date_from,$date_to);
            $post = array(
                'hall'=>$_POST['fltr_hall'],
                'table'=>$_POST['fltr_table'],
                'date_from'=>$_POST['fltr_date_from'],
                'date_to'=>$_POST['fltr_date_to'],
                'hour_from'=>$_POST['fltr_hour_from'],
                'hour_to'=>$_POST['fltr_hour_to']
                );
        }else{
            $results = $this->getDetailedResultPerTable('all','all',$datetime->format('Y-m-d').' 00:00:00',$datetime->format('Y-m-d').' 23:59:59');
            $post = array(
                'hall'=>'all',
                'table'=>'all',
                'date_from'=>$datetime->format('Y-m-d'),
                'date_to'=>$datetime->format('Y-m-d'),
                'hour_from'=>'00',
                'hour_to'=>'23'
                );
        }

        $res = '';
        for($i=0;$i<count($results);$i++){
           switch ($results[$i]['result']) {
                case 'P':
                    $p = $this->translateMessage('LBL_DETAILED_PLAYER');
                    $r_label = '<span class="blue">'.$p.'</span>';
                    break;
                case 'B':
                    $b = $this->translateMessage('LBL_DETAILED_BANKER');
                    $r_label = '<span class="red">'.$b.'</span>';
                    break;
                case 'T':
                    $t = $this->translateMessage('LBL_DETAILED_TIE');
                    $r_label = '<span>'.$t.'</span>';
                    break;
            } 
           
           $shoeCardResult = $this->translateMessage('LBL_DETAILED_SHOE_CARD_RESULT');
            $res .='<td style="display:none;">'.$results[$i]['id'].'</td>
                      <td>'.$results[$i]['code'].'</td>
                      <td>'.$results[$i]['shoeCurrentGameCount'].'</td>
                      <td>'.$results[$i]['shoeCurrentResultCount'].'</td>
                      <td>'.$results[$i]['name'].'</td>
                      <td>'.$results[$i]['date'].'</td>
                      <td>'.$r_label.'</td>
                      <td>'.$results[$i]['bankerPair'].'</td>
                      <td>'.$results[$i]['playerPair'].'</td>
                      <td><button class="btn btn-primary btn-sm cardresult" data-toggle="modal" data-shoe="'.$results[$i]['shoeCurrentResultCount'].'" data-reset="'.$results[$i]['numberOfReset'].'" data-room="'.$results[$i]['roomId'].'" data-target=".bs-example-modal-lg">'.$shoeCardResult.'</button></td>
                    </tr>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:CardResult:index.html.twig',array('data'=>$res,'post'=>$post));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function filterResultAction(){
        echo $_POST['fltr_hall'];
    }
    
    public function getBankerPlayerCardAction(){
        $banker = $this->getBankerCardResult($_POST['shoe'], $_POST['reset'],$_POST['room']);
        $player = $this->getPlayerCardResult($_POST['shoe'], $_POST['reset'],$_POST['room']);
        $url = $this->getRequest()->getSchemeAndHttpHost();

        $player_res = '';
        $bankerCount = 0;
        $playerCount = 0;
        $i_=0;
        for($i=0;$i<count($player);$i++){
            $i_=$i + 1;
            $playerCount += $this->getcardValue($player[$i]['result']);
            if($_SERVER['SERVER_NAME']=='localhost'){
                $player_res.='<div class="card'.$i_.'"><img width="80px;"  src="'. $url.'/CE-Telebet-Backoffice/ap/images/cards/'.$player[$i]['result'].'.png"/></div>';
            }else{
                $player_res.='<div class="card'.$i_.'"><img width="80px;"  src="'. $url.'/ap/images/cards/'.$player[$i]['result'].'.png"/></div>';
            }
        }
        $banker_res = '';
        $e_=0;
        for($e=0;$e<count($banker);$e++){
            $e_=$e + 1;
            $bankerCount += $this->getcardValue($banker[$e]['result']);
            if($_SERVER['SERVER_NAME']=='localhost'){
                $banker_res.='<div class="card'.$e_.'"><img width="80px;"  src="'. $url.'/CE-Telebet-Backoffice/ap/images/cards/'.$banker[$e]['result'].'.png"/></div>';
            }else{
                $banker_res.='<div class="card'.$e_.'"><img width="80px;"  src="'. $url.'/ap/images/cards/'.$banker[$e]['result'].'.png"/></div>';
            }
        }

        $playerCount = (strlen($playerCount)>1)? substr($playerCount,1,1) : $playerCount;
        $bankerCount = (strlen($bankerCount)>1)? substr($bankerCount,1,1) : $bankerCount;

        return new Response($player_res. '#'.$banker_res.'#'.$playerCount.'#'.$bankerCount);
    }
    
    public function getTableAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('email') != ''){
            if($_POST['hall']=='all'){
                $data = $this->getRoomList();
                //print_r($data);
            }else{
                $data = $this->getRoomListByType($_POST['hall']);
            }
        }
        return new Response(json_encode($data));
    }

    public function getcardValue($card){
        $val = substr($card,0,1);

        $res_value = 0;
        if($val=='K' || $val == 'Q' || $val =='J'){
            $res_value = 0;
        }elseif ($val=='A'){
            $res_value =1;
        }else{
            $res_value =$val;
        }

        return $res_value;
    }
}
