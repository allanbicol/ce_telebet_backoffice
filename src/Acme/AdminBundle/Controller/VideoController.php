<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Model;
use Acme\AdminBundle\Entity\Video;

class VideoController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "video");
        $session->set("url", $this->generateUrl("admin_video"));
        
        $type = $this->getRoomType();
        $type_group = '';
        for($i=0;$i<count($type);$i++){
            if($type[$i]['roomType']==1){
                $type_group.= '<optgroup label="MB">';
            }elseif($type[$i]['roomType']==2){
                $type_group.= '<optgroup label="MBV">';
            }else{
                $type_group.= '<optgroup label="MBP">';
            }
            $room = $this->getRoomListByType($type[$i]['roomType']);
            for($e=0;$e<count($room);$e++){
                $type_group.= ' <option value="'.$room[$e]['id'].'">'.$room[$e]['code'].'</option>';
            }
            
            $type_group.= '</optgroup>';
        }
        
        $results = $this->getVideoURL();
        $edit = $this->translateMessage('LBL_VIDEO_EDIT');
        $delete = $this->translateMessage('LBL_VIDEO_DELETE');
        $data = '';
        for($i=0; $i<count($results); $i++){
            $data .= '<tr>
                      <td style="display:none;">'.$results[$i]['id'].'</td>
                      <td>'.$results[$i]['code'].'</td>
                      <td>'.$results[$i]['description'].'</td>
                      <td>'.$results[$i]['country'].'</td>
                      <td>'.$results[$i]['url'].'</td>
                      <td>'.$results[$i]['videoEncoder'].'</td>
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_video_url_edit',array('slug'=>$results[$i]['id'])).'" class="btn btn-primary btn-sm" >'.$edit.'</a> <button data-id="'.$results[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>
                    </tr>';
        }
        
        $device = $this->getDeviceType();
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Video:index.html.twig',array('data'=>$data,'roomType'=>$type_group,'deviceType'=>$device));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function newVideoUrlAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "video");
        $session->set("url", $this->generateUrl("admin_video_url_new"));
        
        $type = $this->getRoomType();
        $type_group = '';
        for($i=0;$i<count($type);$i++){
            if($type[$i]['roomType']==1){
                $type_group.= '<optgroup label="MB">';
            }elseif($type[$i]['roomType']==2){
                $type_group.= '<optgroup label="MBV">';
            }else{
                $type_group.= '<optgroup label="MBP">';
            }
            $room = $this->getRoomListByType($type[$i]['roomType']);
            for($e=0;$e<count($room);$e++){
                $type_group.= ' <option value="'.$room[$e]['id'].'">'.$room[$e]['code'].'</option>';
            }
            
            $type_group.= '</optgroup>';
        }
        
        $device = $this->getDeviceType();
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Video:new.html.twig',array('roomType'=>$type_group,'deviceType'=>$device));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function editVideoUrlAction($slug){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session->set("page_id", "video");
        $session->set("url", $this->generateUrl("admin_video_url_edit",array('slug'=>$slug)));
        
        $type = $this->getRoomType();
        $details = $this->getVideoUrlDetailsById($slug);
        $type_group = '';
        for($i=0;$i<count($type);$i++){
            if($type[$i]['roomType']==1){
                $type_group.= '<optgroup label="MB">';
            }elseif($type[$i]['roomType']==2){
                $type_group.= '<optgroup label="MBV">';
            }else{
                $type_group.= '<optgroup label="MBP">';
            }
            $room = $this->getRoomListByType($type[$i]['roomType']);
            $selected = '';
            for($e=0;$e<count($room);$e++){
                if($room[$e]['id']==$details['roomId']){
                    $type_group.= ' <option selected value="'.$room[$e]['id'].'">'.$room[$e]['code'].'</option>';
                }else{
                    $type_group.= ' <option value="'.$room[$e]['id'].'">'.$room[$e]['code'].'</option>';
                }
                
            }
            
            $type_group.= '</optgroup>';
        }
        
        $device = $this->getDeviceType();

        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Video:edit.html.twig',array('id'=>$slug,'roomType'=>$type_group,'deviceType'=>$device,'details'=>$details));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }

    public function saveVideoUrlAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        
        if($session->get('email') != ''){
            $msg ='';
            if(isset($_POST['new'])){ 
                $model = new Video();
                $msg = $this->translateMessage('LBL_VIDEO_ADDED_SUCCESS');

                //Insert Logs
                $this->insertLogs($session->get('id'),
                    $datetime->format("Y-m-d H:i:s"),
                    'Added video url in '.$_POST['table'].':<br/>URL : '.$_POST['videoUrl']);
            }else{
                $model = $em->getRepository('AcmeAdminBundle:Video')->findOneBy(array('id'=>$_POST['id']));
                $msg = $this->translateMessage('LBL_VIDEO_UPDATED_SUCCESS');

                //Insert Logs
                $this->insertLogs($session->get('id'),
                    $datetime->format("Y-m-d H:i:s"),
                    'Updated video url in '.$_POST['table'].':<br/>URL : '.$_POST['videoUrl']);
            }
            $model->setRoomId($_POST['table']);
            $model->setUrl($_POST['videoUrl']);
            $model->setDeviceTypeId($_POST['device']);
            $model->setCountry($_POST['country']);
            $model->setVideoEncoder($_POST['video_encoder']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                //Call table-setting API after committing to db
                $mobile = $this->getVideoUrlByRoomId($_POST['table'],1);
                $tablet = $this->getVideoUrlByRoomId($_POST['table'],2);
                $desktop = $this->getVideoUrlByRoomId($_POST['table'],3);
                
                $room = $this->getRoomDetailsByRoomId($_POST['table']);
                
                if($room['url']==''){
                    $url =  $this->container->getParameter('default_shoe_server_api');
                }else{
                    $url = $room['url'];
                }
                $api = $this->getAPIServer();
                
                $fields = array(
                        'ROOMID' => urlencode($room['id']),
                        'code' => urlencode($room['code']),
                        'APISERVER' => $api['url'],
                        'label' => $room['label'],
                        'name' => $room['name'],
                        'vip' => $room['vip'],
                        'betMin' => urlencode($room['betMin']),   
                        'betMax' => urlencode($room['betMax']),
                        'tieMin' => urlencode($room['tieMin']),
                        'tieMax' => urlencode($room['tieMax']),
                        'pairMin' => urlencode($room['pairMin']),
                        'pairMax' => urlencode($room['pairMax']),
                        'mobile'=> json_encode($mobile,JSON_UNESCAPED_SLASHES),
                        'tablet'=> json_encode($tablet,JSON_UNESCAPED_SLASHES),
                        'desktop'=> json_encode($desktop,JSON_UNESCAPED_SLASHES) 
                );
                $fields_string = http_build_query($fields);

                $ch = curl_init();
                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                $result = curl_exec($ch);
                //End API call
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
                
             }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
            }
        }
        return $this->redirect($this->generateUrl('admin_video')); 
        
    }
    
    public function getVideoUrlDetailsAction(){
        
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            $this->getVideoUrlDetailsById($_POST['id']);
        }
    }
    
    public function deleteVideoUrlAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM video
            WHERE id=".$_POST['id']."");
        $statement->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted video url.');
        
        $this->get('session')->getFlashBag()->add(
                'success',
                $this->translateMessage('LBL_VIDEO_DELETED_SUCCESSFULLY')
            );
        
        return $this->redirect($this->generateUrl('admin_video')); 
    }
    
   
}
