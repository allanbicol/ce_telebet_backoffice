<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Model;
use Acme\AdminBundle\Controller\GlobalController;

class LoginController extends GlobalController
{
    /**
     * @todo login page
     * @author Allan
     */
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if(isset($_COOKIE['lan'])){
            setcookie("lan", $_COOKIE['lan']);
        }else{
            setcookie("lan", 'en');
        }
        
        if($session->get('email') ==''){
            return $this->render('AcmeAdminBundle:Login:index.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login')); 
        }
    }
    
    /**
     * @todo change language
     * @author Allan
     */
    public function changeLanguageAction(){
        $session = $this->getRequest()->getSession();
        setcookie("lan", $_POST['chooselan']);
        if(isset($_POST['log'])){
            return $this->redirect($this->generateUrl('admin_login'));
        }else{
            return $this->redirect($session->get('url'));
        }
        
    }
    
    
    /**
     * @todo check user account and redirect to Dashboard
     * @author Allan
     */
    public function checkAccountAction(){
        $mod = new Model\GlobalModel();
        $error = 0;
        $message = array();
        $session = $this->getRequest()->getSession();
        /** Start: data validation */
        
        // Check username and password
        if(!isset($_POST['email']) && !isset($_POST['password'])){
            return $this->redirect($this->generateUrl('admin_login'));
        }
        
        if(trim($_POST['email']) == ''){
            $error += 1;
            $message = "Email address should not be blank.";
        }
        
        // check email validity
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
            $error += 1;
            $message = "Email ".$_POST['email']." is not a valid email address.";
        }
        
        if(trim($_POST['password']) == '' && $error == 0){
            $error += 1;
            $message = "Password should not be blank.";
        }
       

        // check user in database
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:AdminUser');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.email, p.fname, p.lname, p.status, p.last_datelogin, p.status")
            ->where(" p.email = :email AND p.password = :password ")
            ->setParameter('email', $_POST['email'])
            ->setParameter('password', $mod->passGenerator($_POST['password']))
            ->getQuery();
        $rows = $query->getArrayResult();

        if(count($rows) < 1 && $error == 0){
            $error += 1;
            $message = $this->translateMessage('LBL_LOGIN_ERROR');
            
            
        }else{
            if(count($rows)==0){
                $error += 1;
                $message = $this->translateMessage('LBL_LOGIN_ERROR');
            }else{
                if ($rows[0]['status'] == 'deactivated'){
                    $error += 1;
                    $message = $this->translateMessage('LBL_LOGIN_DEACTIVATE_ERROR');
                }
            }

        }
        
        /** End: data validation */
        
        if($error == 0){

            $init1 = $rows[0]['fname'];
            $init2 = $rows[0]['lname'];
            // set session
            //if(isset($_POST['remember_me'])){
            //    $this->container->get('session')->migrate($destroy = false, 31536000);
            //}
            // set session
            $session->set('id', $rows[0]['id']); 
            $session->set('email', $rows[0]['email']); 
            $session->set('fname', $rows[0]['fname']); 
            $session->set('lname', $rows[0]['lname']); 
            $session->set('status', $rows[0]['status']); 
            $session->set('last_datelogin', $rows[0]['last_datelogin']);

            $session->set('initial', $init1[0].$init2[0]);

            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("UPDATE adminuser SET last_datelogin=NOW() WHERE id=".$rows[0]['id']);
            $statement->execute();
        
            return $this->redirect($this->generateUrl('admin_dashboard')); 
        }else{
            return $this->render('AcmeAdminBundle:Login:index.html.twig',
                    array('error'=>$message,
                        'email'=>$_POST['email']
                        )
                    );
        }
    }
    
     public function logoutAction(){
        $session = $this->getRequest()->getSession();
        $this->get('session')->clear(); // clear all sessions
        
        return $this->redirect($this->generateUrl('admin_login')); // redirect to login page
        
    }
}
