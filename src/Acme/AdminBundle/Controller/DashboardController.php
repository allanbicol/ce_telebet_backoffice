<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Model\GlobalModel;
use Acme\AdminBundle\Model\BrowserDetection;
use Acme\AdminBundle\Entity\DeviceBlackList;

class DashboardController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $dateFrom = date("Y-m-d", strtotime("-6 day"));
        $dateTo = date("Y-m-d");
        
        $cntCountry = $this->getUserCountByCountry($dateFrom,$dateTo);
        $cc = array();
        foreach ($cntCountry as $key=>$value){
            $cc[strtolower($value['country'])] = $value['COUNT'];
        }
        //$cCountry = json_encode($cc);

        $a = new GlobalModel;
        $agent = new BrowserDetection();
        $cnt = $this->getUserCount($dateFrom,$dateTo);
        $data = $this->getLoginHistory($dateFrom,$dateTo);
        $iosData = $this->getIOSLoginHistory($dateFrom,$dateTo);
        $result = '';
        $iPhone = 0;
        $iPad = 0;
        $Android = 0;
        $chrome = 0;
        $mozilla = 0;
        $safari = 0;
        $handheld = 0;
        $IOSiPhone = 0;
        $IOSiPad = 0;
        $countryResult = '';

        for($i=0; $i<count($data); $i++){
            $brow = $agent->parse_user_agent($data[$i]['browser']);
            //print_r($agent);
            if($brow['platform'] == 'iPhone'){
                $iPhone += 1;
            }
            elseif($brow['platform'] == 'iPad'){
                $iPad += 1;
            }
            elseif($brow['platform'] == 'Android'){
                $Android += 1;
            }
        }

        for($i=0; $i<count($data); $i++){
            $brow = $agent->parse_user_agent($data[$i]['browser']);
            if($brow['browser'] == 'Chrome'){
                $chrome += 1;
            }
            elseif($brow['browser'] == 'Firefox' || $brow['browser'] == 'Mozilla'){
                $mozilla += 1;
            }
            elseif($brow['browser'] == 'Safari'){
                $safari += 1;
            }
            else {
                $handheld += 1;
            }
        }

//      IOS LOGIN HISTORY
        for($i=0; $i<count($iosData); $i++){
            if($iosData[$i]['deviceType'] == 'iPhone'){
                $IOSiPhone += 1;
            }
            elseif($iosData[$i]['deviceType'] == 'iPad'){
                $IOSiPad += 1;
            }
//            elseif($brow['platform'] == 'Android'){
//                $Android += 1;
//            }
        }
        
        $totalCount = $iPhone + $iPad + $Android + $IOSiPhone + $IOSiPad;
        
        $totalIphone = ($iPhone > 0) ? (($iPhone + $IOSiPhone) / $totalCount)* 100 : 0;
        $totalIpad = ($iPad > 0) ? (($iPad + $IOSiPad) / $totalCount) * 100 : 0;
        $totalAndroid = ($Android > 0) ? ($Android / $totalCount ) * 100 : 0;
        
        $totalBrowserCount = $chrome + $mozilla + $safari + $handheld;
        $totalChrome = ($chrome > 0) ? ($chrome / $totalBrowserCount) * 100 : 0;
        $totalMozilla = ($mozilla > 0) ? ($mozilla / $totalBrowserCount) * 100 : 0;
        $totalSafari = ($safari > 0) ? ($safari / $totalBrowserCount) * 100 : 0;
        $totalHandheld = ($handheld > 0) ? ($handheld / $totalBrowserCount) * 100 : 0;

        $data = $this->getDeviceBlackList();
        for($i=0; $i<count($data); $i++){
            $result .= '<tr value="'.$data[$i]['id'].'">
                        <td>'.$data[$i]['id'].'</td>    
                        <td>'.$data[$i]['code'].'</td>
                        <td>'.$data[$i]['Name'].'</td>
                        </tr>';

        }

        $countryData = $this->getUserCountByCountry($dateFrom,$dateTo);
        for($i=0; $i<count($countryData); $i++){

            }
            
            $countryData = $this->getUserCountByCountry($dateFrom,$dateTo);
            for($i=0; $i<count($countryData); $i++){

            $countryResult .= '<tr>   
                            <td>'.$countryData[$i]['country'].'</td>
                            <td>'.$countryData[$i]['COUNT'].'</td>
                            </tr>';

        }

            
        //Client visitors
        $clientResult = '';
        $cnCount = 0;
        $krCount = 0;
        $jpCount = 0;

        for($i=0; $i<count($countryData); $i++){
            if ($countryData[$i]['country']=='CN'){
                $cnCount = $countryData[$i]['COUNT'];
            }
            if ($countryData[$i]['country']=='KR'){
                $krCount = $countryData[$i]['COUNT'];
            }
            if ($countryData[$i]['country']=='JP'){
                $jpCount = $countryData[$i]['COUNT'];
            }
            $clientResult = '<tr>   
                                    <td>China</td>
                                    <td>'.$cnCount.'</td>
                                </tr>
                                <tr>   
                                    <td>Korea</td>
                                    <td>'.$krCount.'</td>
                                </tr>
                                <tr>   
                                    <td>Japan</td>
                                    <td>'.$jpCount.'</td>
                                </tr>
                                ';

        }

        $agent = new BrowserDetection();
        for ($i=6; $i>=0 ; $i--){
            $date = date("Y-m-d", strtotime("-".$i." day"));
            //echo  $date .'<br/>';
            $data = $this->getDeviceLoginHistoryByDate($date);
            $iPhone = 0;
            $iPad = 0;
            $Android = 0;
//            $day7 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day6 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day5 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day4 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day3 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day2 = array('iPhone'=>0,'iPad'=>0,'android'=>0);
//            $day1 = array('iPhone'=>0,'iPad'=>0,'android'=>0);

            for($e=0; $e<count($data); $e++){
                $brow = $agent->parse_user_agent($data[$e]['browser']);
//                print_r($agent);
                if($brow['platform'] == 'iPhone' || $data[$e]['deviceType']=='iPhone'){
                    $iPhone += 1;
                }
                elseif($brow['platform'] == 'iPad' || $data[$e]['deviceType']=='iPad'){
                    $iPad += 1;
                }
                elseif($brow['platform'] == 'Android'){
                    $Android += 1;
                }
            }
            if($i==6){
                $day7 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==5){
                $day6 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==4){
                $day5 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==3){
                $day4 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==2){
                $day3 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==1){
                $day2 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }
            if($i==0){
                $day1 = array('iPhone'=>$iPhone,'iPad'=>$iPad,'android'=>$Android);
            }


        }
        $session->set("page_id", "dashboard");
        $session->set("url", $this->generateUrl("admin_dashboard"));
        
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Dashboard:index.html.twig',array(
                'iPhone'=>round($totalIphone,0),
                'iPad'=>round($totalIpad,0),
                'Android'=>round($totalAndroid,0),
                'Chrome'=>round($totalChrome,0),
                'Mozilla'=>round($totalMozilla,0),
                'Safari'=>round($totalSafari,0),
                'Handheld'=>round($totalHandheld,0),
                'result'=>$result,
                'count'=>$cnt,
                'cntCountry'=>$cc,
                'countryResult'=>$countryResult,
                'browserCount'=>$totalBrowserCount,
                'deviceCount'=>$totalCount,
                'clientResult'=>$clientResult,
                'day1'=>$day1,
                'day2'=>$day2,
                'day3'=>$day3,
                'day4'=>$day4,
                'day5'=>$day5,
                'day6'=>$day6,
                'day7'=>$day7,
                'dateFrom'=>$dateFrom,
                'dateTo'=>$dateTo
            ));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }

    }


}