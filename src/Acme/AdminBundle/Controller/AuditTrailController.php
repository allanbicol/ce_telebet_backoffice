<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\AuditTrail;
use Acme\AdminBundle\Model\GlobalModel;

class AuditTrailController extends GlobalController
{
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "audit_trail");
        $session->set("url", $this->generateUrl("admin_audit_trail"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $a = new GlobalModel;
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $result = '';
        
        if(isset($_POST['btn_function'])){
            $date_from = $_POST['fltr_date_from'].' '.$_POST['fltr_hour_from'].':00:00';
            $date_to = $_POST['fltr_date_to'].' '.$_POST['fltr_hour_to'].':59:59';
            $data = $this->getLogs($date_from,$date_to);
            $result = '';
            for($i=0; $i<count($data); $i++){
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$data[$i]['operatedby'].'</td>
                          <td>'.$data[$i]['operationtime'].'</td>
                          <td>'.$data[$i]['details'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$_POST['fltr_date_from'],
                'date_to'=>$_POST['fltr_date_to'],
                'hour_from'=>$_POST['fltr_hour_from'],
                'hour_to'=>$_POST['fltr_hour_to']
                );
        }else{
            $data = $this->getLogs($datetime->format('Y-m-d').' 00:00:00',$datetime->format('Y-m-d').' 23:59:59');
            $result = '';
            for($i=0; $i<count($data); $i++){
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$data[$i]['operatedby'].'</td>
                          <td>'.$data[$i]['operationtime'].'</td>
                          <td>'.$data[$i]['details'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$datetime->format('Y-m-d'),
                'date_to'=>$datetime->format('Y-m-d'),
                'hour_from'=>'00',
                'hour_to'=>'23'
                );
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:AuditTrail:index.html.twig',array('result'=>$result,'post'=>$post));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
}
