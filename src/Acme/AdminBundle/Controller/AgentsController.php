<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class AgentsController extends Controller
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "agents");
        if($session->get('email') != ''){
            return $this->render('AcmeAdminBundle:Agents:index.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login')); 
        }
    }
}
