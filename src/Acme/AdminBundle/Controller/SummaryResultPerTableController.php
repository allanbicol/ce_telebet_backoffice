<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;

class SummaryResultPerTableController extends GlobalController
{
    public function summaryResultAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        $session->set("page_id", "SummaryResultPerTable");
        $session->set("url", $this->generateUrl("admin_summary_result"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        if(isset($_POST['btn_function'])){
            
            if($_POST['fltr_hall']=='all'){
                $type = $this->getRoomList();
            }else{
                $type = $this->getRoomListByType($_POST['fltr_hall']);
            }
            $post = array(
                'hall'=>$_POST['fltr_hall'],
                'table'=>$_POST['fltr_table'],
                'shoe' =>$_POST['shoe'],
                'date_from'=>$_POST['fltr_date_from'],
                'date_to'=>$_POST['fltr_date_to'],
                'hour_from'=>$_POST['fltr_hour_from'],
                'hour_to'=>$_POST['fltr_hour_to']
                );
        }else{
            $type = $this->getRoomList();  
            $post = array(
                'hall'=>'all',
                'table'=>'all',
                'shoe' =>0,
                'date_from'=>$datetime->format('Y-m-d'),
                'date_to'=>$datetime->format('Y-m-d'),
                'hour_from'=>'00',
                'hour_to'=>'23'
                );
        }
        
        $type1 = '';
        for($i=0;$i<count($type);$i++){
            if(isset($_POST['btn_function'])){ 
                $date_from = $_POST['fltr_date_from'].' '.$_POST['fltr_hour_from'].':00:00';
                $date_to = $_POST['fltr_date_to'].' '.$_POST['fltr_hour_to'].':59:59';
                $result = $this->getSummaryResultPerTable($type[$i]['id'],$_POST['shoe'],$date_from,$date_to);
            }else{
               $result = $this->getSummaryResultPerTable($type[$i]['id'],0,$datetime->format('Y-m-d').' 00:00:00',$datetime->format('Y-m-d').' 23:59:59');
            } 
            $type1.='<tr>
                      <td>'.$type[$i]['id'].'</td>
                      <td>'.$type[$i]['code'].'</td>
                      <td>'.$type[$i]['name'].'</td>
                      <td>'.$type[$i]['label'].'</td>
                      <td>'.$result['@resetCount'].' - '.$result['@shoeCount'].'</td>
                      <td>'.$result['@bCount'].'</td>
                      <td>'.$result['@pCount'].'</td>
                      <td>'.$result['@tCount'].'</td>
                      <td>'.$result['@bpCount'].'</td>
                      <td>'.$result['@ppCount'].'</td>
                    </tr>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));

        $reset = $this->getResetCount();

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:SummaryResultPerTable:index.html.twig',array('data'=>$type1,'reset'=>$reset,'post'=>$post));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    
}
