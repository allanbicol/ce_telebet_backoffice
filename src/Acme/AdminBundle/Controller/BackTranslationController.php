<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Model\GlobalModel;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\AdminSourceTranslation;

class BackTranslationController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "translation");
        $session->set("url", $this->generateUrl("admin_back_translation"));
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('admin_login')); 
        }
        
        $lang = 'en';
        $trans = $this->getLanguageTranslation();
        $data = '';
        $edit = $this->translateMessage('LBL_BACK_EDIT');
        $delete = $this->translateMessage('LBL_BACK_DELETE_TRANSLATION');
        for($i=0; $i<count($trans); $i++){
            $data .= '<tr>
                      <td style="display:none;">'.$trans[$i]['id'].'</td>
                      <td>'.$trans[$i]['category'].'</td>
                      <td>'.$trans[$i]['message'].'</td>
                      <td>'.$trans[$i]['translation'].'</td>
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_back_translation_edit',array('id'=>$trans[$i]['id'])).'" class="btn btn-primary btn-sm" >EDIT</a> <a class="btn btn-danger btn-sm delete" data-id="'.$trans[$i]['id'].'">DELETE</a></div></td>
                    </tr>';
        }
        if(isset($_POST['sel_lang'])){
            $lang = $_POST['sel_lang'];
        }
        
        return $this->render('AcmeAdminBundle:BackTranslation:index.html.twig',array('lang'=>$lang,'data'=>$data));
    }
    
    public function testInfoAction(){
        $a = new GlobalModel;
        echo  $a->getOS('Mozilla/5.0 (Linux; Android 6.0.1; ASUS_Z00LD Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36');
        echo  $a->getBrowser('Mozilla/5.0 (Linux; Android 6.0.1; ASUS_Z00LD Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36');
    }
    
    public function addAction(){

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        
        $session->set("page_id", "translation");
        $session->set("url", $this->generateUrl("admin_back_translation_add"));
        $isActive = $this->checkUserStatus($session->get('id'));
        

        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
        
        return $this->render('AcmeAdminBundle:BackTranslation:new.html.twig');
    }
    
    public function editAction($id){

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        
        $session->set("page_id", "translation");
        $session->set("url", $this->generateUrl("admin_back_translation_edit",array('id'=>$id)));
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
        
        $source = $this->getAdminSourceTranslation($id);
        $result = $this->getAdminTranslation($id);
        
        return $this->render('AcmeAdminBundle:BackTranslation:edit.html.twig',array('id'=>$id,'source'=>$source,'trans'=>$result));
    }
    
    public function saveTranslationAction(){
        $session = $this->getRequest()->getSession();
        
        $id = 0;
        $session->set("page_id", "translation");
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('admin_login')); 
        }
        if(isset($_POST['new'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $model = new AdminSourceTranslation;
            
            $code = $_POST['code'];
            
            $a = $this->getTranslationCode($code);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_BACK_CODE_EXIST')
                );
                  return $this->redirect($this->generateUrl('admin_back_translation_add')); 
              }
              else{
                $model = new AdminSourceTranslation;
              }
            
            $model->setCategory($_POST['category']);
            $model->setMessage($_POST['code']);
            $em->persist($model);
            $em->flush();
            
            $this->insertAdminTranslation($model->getId(), 'en', $_POST['en']);
            $this->insertAdminTranslation($model->getId(), 'cn', $_POST['cn']);
            $this->insertAdminTranslation($model->getId(), 'kr', $_POST['kr']);
            $this->insertAdminTranslation($model->getId(), 'jp', $_POST['jp']);
            $id = $model->getId();
            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                   'success',
                   $this->translateMessage('LBL_BACK_ADDEDD_SUCCESS')
               );
            
        }else{
            if(isset($_POST['en'])){
                $this->saveAdminTranslation($_POST['id'], 'en', $_POST['en']);
            }
            if(isset($_POST['cn'])){
                $this->saveAdminTranslation($_POST['id'], 'cn', $_POST['cn']);
            }
            if(isset($_POST['kr'])){
                $this->saveAdminTranslation($_POST['id'], 'kr', $_POST['kr']);
            }
            if(isset($_POST['jp'])){
                $this->saveAdminTranslation($_POST['id'], 'jp', $_POST['jp']);
            }
            $id = $_POST['id'];
            $this->get('session')->getFlashBag()->add(
                   'success',
                   $this->translateMessage('LBL_BACK_UPDATED_SUCCESS')
               );
        }
        return $this->redirect($this->generateUrl('admin_back_translation_edit',array('id'=>$id))); 
    }
    
    public function deleteTranslationAction(){
       $session = $this->getRequest()->getSession();
        
        $session->set("page_id", "translation");
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
        
        $this->deleteAdminTranslation($_POST['id']);
        
//        $this->get('session')->getFlashBag()->add(
//               'success',
////               'ID-'.$_POST['id'].$this->translateMessage('LBL_BACK_DELETE_TRANSLATION')
//                $this->translateMessage('LBL_BACK_DELETE_TRANSLATION')
//
//           );
        
        return new Response('success'); 
        
    }
   
}
