<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\DeviceBlackList;

class BlacklistController extends GlobalController
{
    public function indexAction() {

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $session->set("page_id", "black_list");
        $session->set("url", $this->generateUrl("admin_device_black"));
        
        $data = $this->getDeviceBlackList();
        
        $result = '';
        $edit = $this->translateMessage('LBL_BLACKDEVICE_EDIT');
        $delete = $this->translateMessage('LBL_BLACKDEVICE_DELETE');
        for($i=0; $i<count($data); $i++){
            $result .= '<form method="post" action="">
                      <tr value="'.$data[$i]['id'].'">
                      <td>'.$data[$i]['id'].'</td>    
                      <td>'.$data[$i]['code'].'</td>
                      <td>'.$data[$i]['Name'].'</td>
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_update_device',array('slug'=>$data[$i]['id'])).'" class="btn btn-primary btn-sm" >'.$edit.'</a> <button data-id="'.$data[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>
                      </tr>
                    </form>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:device.html.twig',array('result'=>$result));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addDeviceAction() {

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $session->set("page_id", "black_list");
        $session->set("url", $this->generateUrl("admin_add_ip_address_blacklist"));

        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:add_device.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addDeviceProcessAction() {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
        
        $code = $_POST['code'];
        $name = $_POST['name'];
        
        if(isset($_POST['new'])){ 
            $a = $this->getBlackListCode($code);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_BLACKDEVICE_DEVICE_CODE_EXIST')
                );
                  return $this->redirect($this->generateUrl('admin_add_device')); 
              }
              else{
              $model = new DeviceBlackList();
              $msg = $this->translateMessage('LBL_BLACKDEVICE_ADDED_SUCCESS');  
              }

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Added '.$name.' device with the code of '.$code.' in the Device Blacklist.');

        }else{
            $model = $em->getRepository('AcmeAdminBundle:DeviceBlackList')->findOneBy(array('id'=>$_POST['id']));
              $msg = $this->translateMessage('LBL_BLACKDEVICE_UPDATED_SUCCESS');

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated '.$name.' device with the code of '.$code.' in the Device Blacklist.');
        }
        
        $model->setCode($code);
        $model->setName($name);
        
        $em->persist($model);
        $em->flush();
        $em->commit();
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
        return $this->redirect($this->generateUrl('admin_device_black')); 
    }
    
    public function updateDeviceAction($slug) {

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
         
        $data = $this->getDeviceDetailsById($slug);
        $isActive = $this->checkUserStatus($session->get('id'));
        $session->set("page_id", "black_list");
        $session->set("url", $this->generateUrl("admin_update_device",array('slug'=>$slug)));


        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:edit_device.html.twig',array('data'=>$data));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
    }

    public function checkBlacklistedDeviceAction(){
//        try {
//            //echo $_POST['uid'];
//            $method = $_SERVER['REQUEST_METHOD'];
//            if (strtolower($method) == 'get') {
//                echo 'Cannot get ' . $http_response_header();
//            } else {
//                echo $http_response_header();
//            }
//        }catch (Exception $e){
//            echo $e->getMessage();
//        }
//        $response = array('blacklist'=>1);
//        header('Content-Type: application/json');
        //echo json_encode($response);
        //return new Response(json_encode($response));

    }

    public function deleteBlackDeviceAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        if($session->get('email') == ''){
            return new Response("session expired");
        }

        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }

        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted device in the Device Blacklist.');

        $statement = $connection->prepare("DELETE FROM blacklistDevice
            WHERE id=".$_POST['id']."");
        $statement->execute();

//        $this->get('session')->getFlashBag()->add(
//            'success',
////                $this->translateMessage('LBL_DEVICE_DELETE_SUCCESS')
//            $msg = $this->translateMessage('LBL_BLACKDEVICE_DELETED_SUCCESS')
//        );

        return $this->redirect($this->generateUrl('admin_device_black'));
    }
}
