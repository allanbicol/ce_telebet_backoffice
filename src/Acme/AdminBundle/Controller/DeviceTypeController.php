<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\DeviceType;

class DeviceTypeController extends GlobalController
{
    public function indexAction(){

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
        
        $session->set("page_id", "othersetting");
        $session->set("url", $this->generateUrl("admin_device_type"));

        $edit = $this->translateMessage('LBL_DEVICE_EDIT');
        $delete = $this->translateMessage('LBL_DEVICE_DELETE');
        $results = $this->getDeviceType();
        $data = '';
        for($i=0; $i<count($results); $i++){
            $data .= '<tr>
                      <td>'.$results[$i]['id'].'</td>
                      <td>'.$results[$i]['name'].'</td>
                      <td>'.$results[$i]['description'].'</td>
                      <td><div style="width:200px"><a href="'.$this->generateUrl('admin_device_type_edit',array('id'=>$results[$i]['id'])).'" class="btn btn-primary btn-sx" >'.$edit.'</a> <button data-id="'.$results[$i]['id'].'" class="btn btn-danger btn-sx delete">'.$delete.'</button></div></td>
                    </tr>';
        }
        
        return $this->render('AcmeAdminBundle:DeviceType:index.html.twig',array('data'=>$data));
    
    }
    
    public function newAction(){

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }  
        $session->set("page_id", "othersetting");
        $session->set("url", $this->generateUrl("admin_device_type_new"));
        
        return $this->render('AcmeAdminBundle:DeviceType:new.html.twig');
    }
    
    public function editAction($id){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('admin_login')); 
        }  
        $session->set("page_id", "othersetting");
        $session->set("url", $this->generateUrl("admin_device_type_edit",array('id'=>$id)));
        $type = $this->getDeviceTypeById($id);
        
        return $this->render('AcmeAdminBundle:DeviceType:edit.html.twig',array('type'=>$type));
    }
    
    public function addupdateDeviceTypeAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $name = $_POST['name'];
        
        $session->set("page_id", "translation");
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('admin_login')); 
        }
        $msg = '';
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
            
        if(isset($_POST['new'])){

            
            $a = $this->getDeviceTypeName($name);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_DEVICE_NAME_EXIST')
                );
                  return $this->redirect($this->generateUrl('admin_device_type_new')); 
              }
              else{
                $model = new DeviceType;   
                $msg = $this->translateMessage('LBL_DEVICE_ADD_SUCCESS'); 
              }

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Added device type name '.$_POST['name']);

        }else{
            $model = $em->getRepository('AcmeAdminBundle:DeviceType')->findOneBy(array('id'=>$_POST['id']));
            $msg = $this->translateMessage('LBL_DEVICE_UPDATE_SUCCESS');

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated device type name '.$_POST['name']);
        }
        
        $model->setName($_POST['name']);
        $model->setDescription($_POST['description']);

        $em->persist($model);
        $em->flush();

        $em->getConnection()->commit();
        
        $this->get('session')->getFlashBag()->add(
            'success',
            $msg
        );
        
        if(isset($_POST['new'])){
            return $this->redirect($this->generateUrl('admin_device_type')); 
        }else{
            return $this->redirect($this->generateUrl('admin_device_type_edit',array('id'=>$_POST['id']))); 
        }
    }
    
    public function deleteDeviceTypeAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM devicetype
            WHERE id=".$_POST['id']."");
        $statement->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted device type');

        $this->get('session')->getFlashBag()->add(
                'success',
                $this->translateMessage('LBL_DEVICE_DELETE_SUCCESS')
                
            );
        
        return $this->redirect($this->generateUrl('admin_device_type')); 
    }
}
