<?php

namespace Acme\AdminBundle\Controller;

use Acme\AdminBundle\Entity\Logs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class GlobalController extends Controller
{
    // Get room functions ============================
    public function getRoomList(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('p')
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
    }

    public function getRoomType(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('p')
            ->select("p.roomType")
            ->groupby("p.roomType")
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getRoomListByType($type){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('p')
            ->where("p.roomType =:type")
            ->setParameter("type", $type)
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getRoomDetailsByRoomId($roomId){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('r')
            ->select("r.id,r.code,r.label,r.name,r.vip,r.roomType,r.cameraIp,r.betMax,r.betMin,r.tieMax,r.tieMin,r.pairMax,r.pairMin,s.url")
            ->leftJoin('AcmeAdminBundle:Shoe','s','WITH','s.roomId = r.id')
            ->where("r.id =:id")
            ->setParameter("id", $roomId)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    // Table Setting functions ============================
    public function getTableSettingList(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('r')
            ->select("r.id,r.code,r.label,r.name,r.roomType,r.cameraIp,r.betMax,r.betMin,r.tieMax,r.tieMin,r.pairMax,r.pairMin,a.message")
            ->leftJoin('AcmeAdminBundle:Announcement', 'a', 'WITH', 'a.roomId = r.id')
            ->orderBy('r.id', 'ASC')
            ->groupBy('r.id')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getTableSettingListById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Room');
        $query = $cust->createQueryBuilder('r')
            ->select("r.id,r.code,r.label,r.name,r.vip,r.roomType,r.cameraIp,r.betMax,r.betMin,r.tieMax,r.tieMin,r.pairMax,r.pairMin,a.message,a.roomId,s.url")
            ->leftJoin('AcmeAdminBundle:Announcement', 'a', 'WITH', 'a.roomId = r.id')
            ->leftJoin('AcmeAdminBundle:Shoe','s','WITH','s.roomId = r.id')
            ->where("r.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    public function AnnouncementByTable($roomId){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Announcement');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id,p.message,p.roomId,p.languageId,a.code,a.description")
            ->leftJoin('AcmeAdminBundle:Language', 'a', 'WITH', 'a.id = p.languageId')
            ->where("p.roomId =:id")
            ->setParameter("id", $roomId)
            ->getQuery();

        return $query->getArrayResult();
    }
    
    public function getTableSettingCodeAndName($code,$tableName) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM room WHERE code = '".$code."' OR name = '".$tableName."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }

    //DeviceType functions ====================================

    public function getDeviceType(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:DeviceType');
        $query = $cust->createQueryBuilder('p')
            ->getQuery();
        return  $query->getArrayResult();
    }

    public function getDeviceTypeById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:DeviceType');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }
    
    public function getDeviceTypeName($name) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM devicetype WHERE name = '".$name."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }



    //Video URL functions
    public function getVideoURL(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Video');
        $query = $cust->createQueryBuilder('v')
            ->select("v.id,v.url,d.description,v.country,r.code,r.name,r.vip,v.videoEncoder")
            ->leftJoin('AcmeAdminBundle:Room', 'r', 'WITH', 'v.roomId = r.id')
            ->leftJoin('AcmeAdminBundle:DeviceType', 'd', 'WITH', 'v.deviceTypeId = d.id')
            ->orderBy('v.roomId', 'ASC')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getVideoUrlDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Video');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }
    public function getVideoUrlByRoomId($id,$device_id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Video');
        $query = $cust->createQueryBuilder('p')
            ->select("p.url")
            ->where("p.roomId =:id and p.deviceTypeId =:deviceId")
            ->setParameter("id", $id)
            ->setParameter("deviceId", $device_id)
            ->getQuery();
        $output = $query->getArrayResult();
        return $output;
    }
    //Summary Result Per Table
    public function getSummaryResultPerTable($roomId,$shoe,$dateFrom,$dateTo){
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $proc = $connection->prepare("CALL spGetSummaryResultPerTable('".$roomId."',".$shoe.",'".$dateFrom."','".$dateTo."', @bCount,@pCount,@tCount,@ppCount,@bpCount,@resetCount,@shoeCount)");
        $proc->execute();

        $statement = $connection->prepare("select @bCount,@pCount,@tCount,@ppCount,@bpCount,@resetCount,@shoeCount");
        $statement->execute();
        $output = $statement->fetchAll();

        return (count($output) > 0) ? $output[0] : array();
    }

    //Detailed Result Per Table
    public function getDetailedResultPerTable($roomType,$roomId,$dateFrom,$dateTo){
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $s_room_type = '';
        if($roomType!='all'){
            $s_room_type = " AND r.roomType =".$roomType."";
        }
        $s_room_id = '';
        if($roomId!='all'){
            $s_room_id = " AND g.roomId =".$roomId."";
        }

        $statement = $connection->prepare("SELECT g.id,r.code,g.shoeCurrentGameCount,g.shoeCurrentResultCount,r.name,g.date,g.result,g.bankerPair,g.playerPair,g.numberOfReset,g.roomId FROM gamebeadroadresult g LEFT JOIN room r ON r.id = g.roomId WHERE g.date BETWEEN '".$dateFrom."' AND '".$dateTo."' $s_room_type  $s_room_id ");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function getBankerCardResult($shoe,$reset,$room){
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT result FROM cardresult WHERE hand='banker' AND numberOfReset=".$reset." AND shoeCurrentResultCount = ".$shoe." AND roomId=".$room."");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function getPlayerCardResult($shoe,$reset,$room){
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT result FROM cardresult WHERE hand='player' AND numberOfReset=".$reset." AND shoeCurrentResultCount = ".$shoe."  AND roomId=".$room."");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function getLanguageTranslation()
    {
        //$session = $this->getRequest()->getSession();
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("select ast.id,ast.category,ast.message,adt.language,adt.translation from adminSourceTranslation ast left join adminTranslation adt on adt.id=ast.id where adt.language='en'");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function connectMongo(){
        $hosts = $this->container->getParameter('mongo_host');
        $port = $this->container->getParameter('mongo_port');
        $database = $this->container->getParameter('mongo_db');
        $username= $this->container->getParameter('mongo_user');
        $password= $this->container->getParameter('mongo_pass');

        $connecting_string =  sprintf('mongodb://%s:%d/%s', $hosts, $port,$database);
        $connection =  new \MongoClient($connecting_string,array('username'=>$username,'password'=>$password));

        return $connection;
    }

    public function getAdminTranslation($id){
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("select ast.id,ast.category,ast.message,adt.language,adt.translation from adminSourceTranslation ast left join adminTranslation adt on adt.id=ast.id where adt.id='".$id."'");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function getAdminSourceTranslation($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:AdminSourceTranslation');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    public function insertAdminTranslation($id,$lang,$message){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("INSERT INTO adminTranslation (id,language,translation) VALUES ('".$id."','".$lang."','".$message."')");
        $statement->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Add Translation: <br/>Language : '.$lang.'<br/>Message : '.$message);

        return 'done';
    }

    public function saveAdminTranslation($id,$lang,$message){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("UPDATE adminTranslation SET translation='".$message."' where id=".$id." and language='".$lang."'");
        $statement->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Update Translation: <br/>Language : '.$lang.'<br/>Message : '.$message);

        return 'done';
    }
    
    public function getTranslationCode($code) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM adminSourceTranslation WHERE message = '".$code."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }

    public function deleteAdminTranslation($id){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement1 = $connection->prepare("DELETE FROM adminSourceTranslation where id='".$id."'");
        $statement1->execute();

        $statement2 = $connection->prepare("DELETE FROM adminTranslation where id='".$id."'");
        $statement2->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted Translation.');

        return 'done';
    }

    public function getAPIServer(){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Server');
        $query = $cust->createQueryBuilder('p')
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    //Admin User Function -Cath    
    public function getAdminUser() {
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:AdminUser');
        $query = $cust->createQueryBuilder('a')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getAdminUserDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:AdminUser');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    public function translateMessage($code){

        if(isset($_COOKIE['lan'])){
            $lang = $_COOKIE['lan'];
        }else{
            setcookie("lan", 'cn');
            $lang = 'cn';
        }
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT translation FROM adminSourceTranslation ast LEFT JOIN adminTranslation adt ON ast.id = adt.id WHERE message='".$code."' AND language='".$lang."'");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output[0]['translation'];
    }
    
    public function getAdminUserEmail($email) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM adminuser WHERE email = '".$email."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }

    //Device Blacklist Function -Cath

    public function getDeviceBlackList() {
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:DeviceBlackList');
        $query = $cust->createQueryBuilder('a')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getDeviceDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:DeviceBlackList');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }

    public function getBlackListCode($code) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM blacklistDevice WHERE code = '".$code."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }

    //Login History Function -Cath

    public function getLoginHistory($dateFrom,$dateTo) {
        $date_From = $dateFrom . ' 00:00:00';
        $date_To   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $statement = $connection->prepare("SELECT * FROM loginhistory WHERE date BETWEEN '".$date_From."' AND '".$date_To."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output;
    }

    public function getUserCount($dateFrom,$dateTo) {
        $date_From = $dateFrom . ' 00:00:00';
        $date_To   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $statement = $connection->prepare("SELECT COUNT(cnt) AS COUNT FROM (SELECT COUNT(country) AS cnt FROM loginhistory WHERE DATE BETWEEN '".$date_From."' AND '".$date_To."' GROUP BY country,ipAddress) AS tbl");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['COUNT'];
    }

    public function getUserCountByCountry($dateFrom,$dateTo) {
        $date_From = $dateFrom . ' 00:00:00';
        $date_To   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $statement = $connection->prepare("SELECT country,COUNT(cnt) AS COUNT FROM (SELECT COUNT(ipAddress) AS cnt,country FROM loginhistory WHERE DATE BETWEEN '".$date_From."' AND '".$date_To."' AND country<>'null' GROUP BY ipAddress,country) AS tbl GROUP BY country");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output;
    }

    //IOS Login History Function -Cath
    public function getIOSLoginHistory($dateFrom,$dateTo) {
        $date_From = $dateFrom . ' 00:00:00';
        $date_To   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();
        $statement = $connection->prepare("SELECT * FROM iosLoginHistory WHERE dateLogin BETWEEN '".$date_From."' AND '".$date_To."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output;
    }

    public function getDeviceLoginHistoryByDate($date){
        $date_from = $date . ' 00:00:00';
        $date_to   = $date . ' 23:59:59';

        $connection    = $this->getDoctrine()->getManager()->getConnection();
        //$count = $connection->prepare("SELECT browser,ipAddress,country FROM loginhistory WHERE date BETWEEN '".$date_from."' AND '".$date_to."'")->rowCount();
        $statement = $connection->prepare("(SELECT browser,ipAddress,country,date,'' as deviceType FROM loginhistory WHERE date BETWEEN '".$date_from."' and '".$date_to."')
                                            UNION
                                            (SELECT '',ip,'',dateLogin,deviceType FROM iosLoginHistory WHERE dateLogin BETWEEN '".$date_from."' and '".$date_to."')");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output;

    }

    //Web App Function -Cath
    public function getLoginHistoryByDate($dateFrom,$dateTo) {
        $date_from = $dateFrom . ' 00:00:00';
        $date_to   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT * FROM loginhistory WHERE date BETWEEN '".$dateFrom."' AND '".$dateTo."' order by date desc");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    //Native App Function -Cath
    public function getIOSLoginHistoryByDate($dateFrom,$dateTo) {
        $date_from = $dateFrom . ' 00:00:00';
        $date_to   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT * FROM iosLoginHistory WHERE dateLogin BETWEEN '".$dateFrom."' AND '".$dateTo."' order by dateLogin desc");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    //Telebet User Function -Cath
    public function getTelebetUser() {
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:User');
        $query = $cust->createQueryBuilder('a')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getTelebetUserDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:User');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }
    
    public function getTelebetUserUsername($username) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM user WHERE username = '".$username."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }

    //IP Address Blacklist Function -Cath
    
    public function getIpBlackList() {
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:IpBlacklist');
        $query = $cust->createQueryBuilder('a')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getIpAddressDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:IpBlacklist');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:id")
            ->setParameter("id", $id)
            ->getQuery();
        $output = $query->getArrayResult();
        return (count($output) > 0) ? $output[0] : array();
    }
    
    //Audit Trail Function -Cath
    
    public function insertLogs($operatedBy,$operationTime,$details){

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $mod = new Logs();
        
        $mod->setOperatedby($operatedBy);
        $mod->setOperationtime($operationTime);
        $mod->setDetails($details);
        
        $em->persist($mod);
        $em->flush();

        $em->getConnection()->commit();

        return 'done';
    }
    
    public function getLogs($dateFrom,$dateTo) {
        $date_from = $dateFrom . ' 00:00:00';
        $date_to   = $dateTo . ' 23:59:59';
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT l.id,details,operationtime,CONCAT(a.`fname`,' ',a.`lname`) as operatedby FROM logs l LEFT JOIN adminuser a ON a.`id` = l.`operatedby` WHERE operationtime BETWEEN '".$date_from."' AND '".$date_to."' order by operationtime desc");
        $statement->execute();
        $output = $statement->fetchAll();

        return $output;
    }

    public function checkUserStatus($id){
        $connection = $this->getDoctrine()->getManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) as cnt FROM adminuser WHERE id = '".$id."' and status='active'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }
    
    //Language -Cath
    
    public function getLanguage() {
        $cust = $this->getDoctrine()->getRepository('AcmeAdminBundle:Language');
        $query = $cust->createQueryBuilder('a')
            ->getQuery();
        return $query->getArrayResult();
    }

    public function getResetCount(){
        $connection = $this->getDoctrine()->getManager()->getConnection();

        $statement = $connection->prepare("SELECT numberOfReset FROM gamebeadroadresult GROUP BY numberOfReset");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output;
    }

    public function getAdminUserEmailByID($id){
        $connection = $this->getDoctrine()->getManager()->getConnection();

        $statement = $connection->prepare("SELECT email AS email FROM adminuser WHERE id = $id");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['email'];
    }

    public function getTableCode($code) {
        $connection = $this->getDoctrine()->getEntityManager()->getConnection();

        $statement = $connection->prepare("SELECT COUNT(id) AS cnt FROM room WHERE code = '".$code."'");
        $statement->execute();
        $output = $statement->fetchAll();
        return $output[0]['cnt'];
    }
}