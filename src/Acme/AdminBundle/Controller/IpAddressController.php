<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\IpBlacklist;

class IpAddressController extends GlobalController
{
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "black_list_ip");
        $session->set("url", $this->generateUrl("admin_ip_address_blacklist"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $data = $this->getIpBlackList();
        
        $result = '';
        $edit = $this->translateMessage('LBL_BLACKIP_EDIT');
        $delete = $this->translateMessage('LBL_BLACKIP_DELETE');
        for($i=0; $i<count($data); $i++){
            $result .= '<form method="post" action="">
                      <tr value="'.$data[$i]['id'].'">
                      <td class="hidden">'.$data[$i]['id'].'</td>    
                      <td>'.$data[$i]['ipfrom'].'</td>
                      <td>'.$data[$i]['ipto'].'</td>
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_update_ip_address_blacklist',array('slug'=>$data[$i]['id'])).'" class="btn btn-primary btn-sm" >'.$edit.'</a> <button data-id="'.$data[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>
                      </tr>
                    </form>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:ipAddress.html.twig',array('result'=>$result));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addIpAddressAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "black_list");
        $session->set("url", $this->generateUrl("admin_add_ip_address_blacklist"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:add_ip_address.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addIpAddressProcessAction() {
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
        
        $ipfrom = $_POST['ipfrom'];
        $ipto = $_POST['ipto'];
        
        if(isset($_POST['new'])){ 
           
              $model = new IpBlacklist();
              $msg = $this->translateMessage('LBL_BLACKIP_ADDED_SUCCESS');
              
        }else{
            $model = $em->getRepository('AcmeAdminBundle:IpBlacklist')->findOneBy(array('id'=>$_POST['id']));
            $msg = $this->translateMessage('LBL_BLACKIP_UPDATED_SUCCESS');
        }
        
        $model->setIpfrom($ipfrom);
        $model->setIpto($ipto);
        
        $em->persist($model);
        $em->flush();
        $em->commit();
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
        return $this->redirect($this->generateUrl('admin_ip_address_blacklist')); 
    }
    
    public function updateIpAddressAction($slug) {
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session->set("page_id", "black_list");
        $session->set("url", $this->generateUrl("admin_update_ip_address_blacklist",array('slug'=>$slug)));

        $data = $this->getIpAddressDetailsById($slug);
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Blacklist:edit_ip_address.html.twig',array('data'=>$data));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
    }
    
    public function deleteBlackIpAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        if($session->get('email') == ''){
            return new Response("session expired");
        }

        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }

        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted ip address in the IP Address Blacklist.');

        $statement = $connection->prepare("DELETE FROM blacklistIP
            WHERE id=".$_POST['id']."");
        $statement->execute();

//        $this->get('session')->getFlashBag()->add(
//            'success',
////                $this->translateMessage('LBL_DEVICE_DELETE_SUCCESS')
//            $msg = $this->translateMessage('LBL_BLACKDEVICE_DELETED_SUCCESS')
//        );

        return $this->redirect($this->generateUrl('admin_device_black'));
    }
    
}
