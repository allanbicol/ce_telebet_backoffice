<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Model\GlobalModel;
use Acme\AdminBundle\Model\BrowserDetection;
use Acme\AdminBundle\Entity\LoginHistory;
use Acme\AdminBundle\Entity\IOSLoginHistory;

class LoginHistoryController extends GlobalController
{
    public function webAppAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "login_history");
        $session->set("url", $this->generateUrl("admin_login_history_webapp"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $a = new GlobalModel;
        $agent = new BrowserDetection();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $result = '';
        
        if(isset($_POST['btn_function'])){
            $date_from = $_POST['fltr_date_from'].' '.$_POST['fltr_hour_from'].':00:00';
            $date_to = $_POST['fltr_date_to'].' '.$_POST['fltr_hour_to'].':59:59';
            $data = $this->getLoginHistoryByDate($date_from,$date_to);
            $result = '';
            for($i=0; $i<count($data); $i++){
                $brow = $agent->parse_user_agent($data[$i]['browser']);
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$brow['browser'].'</td>
                          <td>'.$brow['platform'].'</td>
                          <td>'.$data[$i]['ipAddress'].'</td>
                          <td>'.$data[$i]['country'].'</td>
                          <td>'.$data[$i]['date'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$_POST['fltr_date_from'],
                'date_to'=>$_POST['fltr_date_to'],
                'hour_from'=>$_POST['fltr_hour_from'],
                'hour_to'=>$_POST['fltr_hour_to']
                );
        }else{
            $data = $this->getLoginHistoryByDate($datetime->format('Y-m-d').' 00:00:00',$datetime->format('Y-m-d').' 23:59:59');
            $result = '';
            for($i=0; $i<count($data); $i++){
                $brow = $agent->parse_user_agent($data[$i]['browser']);
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$brow['browser'].'</td>
                          <td>'.$brow['platform'].'</td>
                          <td>'.$data[$i]['ipAddress'].'</td>
                          <td>'.$data[$i]['country'].'</td>
                          <td>'.$data[$i]['date'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$datetime->format('Y-m-d'),
                'date_to'=>$datetime->format('Y-m-d'),
                'hour_from'=>'00',
                'hour_to'=>'23'
                );
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:LoginHistory:web_app.html.twig',array('result'=>$result,'post'=>$post));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function nativeAppAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "login_history");
        $session->set("url", $this->generateUrl("admin_login_history_nativeapp"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
         if(isset($_POST['btn_function'])){
            
            $date_from = $_POST['fltr_date_from'].' '.$_POST['fltr_hour_from'].':00:00';
            $date_to = $_POST['fltr_date_to'].' '.$_POST['fltr_hour_to'].':59:59';
            $data = $this->getIOSLoginHistoryByDate($date_from,$date_to);
            $result = '';
            for($i=0; $i<count($data); $i++){
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$data[$i]['deviceType'].'</td>
                          <td>'.$data[$i]['uid'].'</td>
                          <td>'.$data[$i]['dateLogin'].'</td>
                          <td>'.$data[$i]['ip'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$_POST['fltr_date_from'],
                'date_to'=>$_POST['fltr_date_to'],
                'hour_from'=>$_POST['fltr_hour_from'],
                'hour_to'=>$_POST['fltr_hour_to']
                );
        }else{
            $data = $this->getIOSLoginHistoryByDate($datetime->format('Y-m-d').' 00:00:00',$datetime->format('Y-m-d').' 23:59:59');
            $result = '';
            for($i=0; $i<count($data); $i++){
                $result .= '
                          <tr>
                          <td class="hidden">'.$data[$i]['id'].'</td>
                          <td>'.$data[$i]['deviceType'].'</td>
                          <td>'.$data[$i]['uid'].'</td>
                          <td>'.$data[$i]['dateLogin'].'</td>
                          <td>'.$data[$i]['ip'].'</td>
                          </tr>';
            }
            $post = array(
                'date_from'=>$datetime->format('Y-m-d'),
                'date_to'=>$datetime->format('Y-m-d'),
                'hour_from'=>'00',
                'hour_to'=>'23'
                );
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:LoginHistory:native_app.html.twig',array('result'=>$result,'post'=>$post));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
}
