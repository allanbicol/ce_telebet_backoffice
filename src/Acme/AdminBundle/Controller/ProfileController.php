<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\AdminUser;
use Acme\AdminBundle\Model\GlobalModel;

class ProfileController extends GlobalController
{
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        $session->set("url", $this->generateUrl("admin_user_profile"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:Profile:profile.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function editUserProfileAction() {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
        $mod = new GlobalModel;
        
        $email = $_POST['email'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $status = $_POST['status'];
        $password = $mod->passGenerator($_POST['password']); 
        
        $model = $em->getRepository('AcmeAdminBundle:AdminUser')->findOneBy(array('id'=>$_POST['id']));
        
        $model->setFname($fname);
        $model->setLname($lname);
        $model->setEmail($email);
        if($_POST['password']!=''){
            $model->setPassword($password);
        }
        $model->setStatus($status);
        
        $em->persist($model);
        $em->flush();
        $em->commit();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Updated user '.$_POST['id'].' profile: <br/> email : '.$_POST['email'].'<br/>lastname : '.$_POST['lname'].'<br/>firstname : '.$_POST['fname']);
        
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->translateMessage('LBL_PROFILE_SUCCESS_UPDATED')
                );
        return $this->redirect($this->generateUrl('admin_user'));
    }
}
