<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use \Acme\AdminBundle\Entity\User;
use Acme\AdminBundle\Model\GlobalModel;

class UserController extends GlobalController
{
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "telebet_user");
        $session->set("url", $this->generateUrl("admin_telebet_user"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $data = $this->getTelebetUser();
        $result = '';
        $edit = $this->translateMessage('LBL_TELEBETUSER_EDIT');
        $delete = $this->translateMessage('LBL_TELEBETUSER_DELETE');
        for($i=0; $i<count($data); $i++){
            $result .= '<form method="post" action="">
                      <tr value="'.$data[$i]['id'].'">
                      <td class="hidden">'.$data[$i]['id'].'</td>
                      <td>'.$data[$i]['username'].'</td>
                      <td>'.$data[$i]['balance'].'</td>   
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_update_telebet_user',array('slug'=>$data[$i]['id'])).'" class="btn btn-primary btn-sm" >'.$edit.'</a> <button data-id="'.$data[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>    
                      </tr>
                    </form>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:User:user.html.twig',array('result'=>$result));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addUserAction() {

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $session->set("page_id", "telebet_user");
        $session->set("url", $this->generateUrl("admin_add_telebet_user"));

        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:User:add_user.html.twig');
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
    }
    
    public function editTelebetUserAction($slug){

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session = $this->getRequest()->getSession();
        $session->set("page_id", "telebet_user");
        $session->set("url", $this->generateUrl("admin_update_telebet_user",array('slug'=>$slug)));
         
        $data = $this->getTelebetUserDetailsById($slug);
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:User:edit_user.html.twig',array('data'=>$data));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        } 
    }
    
    public function addeditTelebetUserAction() {
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $session = $this->getRequest()->getSession();

        $em = $this->getDoctrine()->getEntityManager();
        $em->getConnection()->beginTransaction();
        $mod = new GlobalModel;
        
        $username = $_POST['username'];
        $balance = $_POST['balance'];
        if($_POST['password'] != ''){
            //Call table-setting API after committing to db
            $api = $this->getAPIServer();
            $url = $api['pass_gen_api'];
            $fields = array(
                'password' => urlencode($_POST['password'])
            );
            $fields_string = http_build_query($fields);

            $ch = curl_init();
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            $da = json_decode($result,true);

            $password = $da['data'];
        }

        
        if(isset($_POST['new'])){ 

            $a = $this->getTelebetUserUsername($username);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_TELEBETUSER_USERNAME_EXIST')
                );
                  return $this->redirect($this->generateUrl('admin_add_telebet_user')); 
              }
              else{
                $model = new User();
                $msg = $this->translateMessage('LBL_TELEBETUSER_ADDED_SUCCESSFULLY');
              }
            
            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Added new user '.$_POST['username'].':<br/>Balance : '.$_POST['balance']);
        }else{
            $model = $em->getRepository('AcmeAdminBundle:User')->findOneBy(array('id'=>$_POST['id']));
            $msg = $this->translateMessage('LBL_TELEBETUSER_UPDATED_SUCCESSFULLY');

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated user <br/>Username : '.$_POST['username'].'<br/>Balance : '.$_POST['balance']);
        }
        
        $model->setUsername($username);
        $model->setBalance($balance);
        if($_POST['password']!=''){
            $model->setPassword($password);
        }
        $em->persist($model);
        $em->flush();
        $em->commit();
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
        return $this->redirect($this->generateUrl('admin_telebet_user'));
    }
    
    public function deleteTelebetUserAction(){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') == '' && $isActive==0){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM user
            WHERE id=".$_POST['id']."");
        $statement->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted telebet user.');

        $this->get('session')->getFlashBag()->add(
                'success',
                'DELETED'
            );
        
        return $this->redirect($this->generateUrl('admin_device_black')); 
    }
    
}