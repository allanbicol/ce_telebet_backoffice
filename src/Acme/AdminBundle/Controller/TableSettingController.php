<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Controller\GlobalController;
use Acme\AdminBundle\Entity\Room;
use Acme\AdminBundle\Entity\Announcement;

class TableSettingController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $session->set("page_id", "tableSetting");
        $session->set("url", $this->generateUrl("admin_table_setting"));
        
        $results = $this->getTableSettingList();
        $data = '';
        $edit = $this->translateMessage('LBL_SETTING_EDIT');
        $delete = $this->translateMessage('LBL_SETTING_DELETE');
        for($i=0; $i<count($results); $i++){
            $data .= '<tr>
                      <td style="display:none;">'.$results[$i]['id'].'</td>
                      <td>'.$results[$i]['code'].'</td>
                      <td>'.$results[$i]['name'].'</td>
                      <td>'.$results[$i]['label'].'</td>
                      <td>'.$results[$i]['message'].'</td>
                      <td>'.$results[$i]['betMin'].'</td>
                      <td>'.$results[$i]['betMax'].'</td>
                      <td>'.$results[$i]['tieMin'].'</td>
                      <td>'.$results[$i]['tieMax'].'</td>
                      <td>'.$results[$i]['pairMin'].'</td>
                      <td>'.$results[$i]['pairMax'].'</td>
                      <td><div style="width:150px;"><a href="'.$this->generateUrl('admin_table_setting_edit',array('slug'=>$results[$i]['id'])).'" class="btn btn-primary btn-sm" >'.$edit.'</a> <button data-id="'.$results[$i]['id'].'" class="btn btn-danger btn-sm delete">'.$delete.'</button></div></td>
                    </tr>';
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:TableSetting:index.html.twig',array('data'=>$data));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function addTableSettingAction(){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "tableSetting");
        $session->set("url", $this->generateUrl("admin_table_setting_add"));
        
        $isActive = $this->checkUserStatus($session->get('id'));
        
        $announcement = $this->getLanguage();
        
        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:TableSetting:add.html.twig',array('announcement'=>$announcement));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }


    public function editTableSettingAction($slug){
        $session = $this->getRequest()->getSession();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $session->set("page_id", "tableSetting");
        $session->set("url", $this->generateUrl("admin_table_setting_edit",array('slug'=>$slug)));

        $result = $this->getTableSettingListById($slug);
        
        $announcement = $this->AnnouncementByTable($result['roomId']);
        $isActive = $this->checkUserStatus($session->get('id'));

        if($session->get('email') != '' && $isActive==1){
            return $this->render('AcmeAdminBundle:TableSetting:edit.html.twig',array('tableSetting'=>$result,'announcement'=>$announcement));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    public function saveTableSettingAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $msgId = $_POST['msgId'];
        $msgContent = $_POST['msg'];

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        
        if($session->get('email') != ''){

            if(isset($_POST['new'])){
                $a = $this->getTableCode($_POST['code']);
                if($a > 0){
                    $this->get('session')->getFlashBag()->add(
                        'danger',
                        $this->translateMessage('LBL_SETTING_CODE_EXIST')
                    );
                    return $this->redirect($this->generateUrl('admin_user_add'));
                }
                else{
                    $model = new Room();
                    $msg = $this->translateMessage('LBL_SETTING_ADDED_SUCCESS');

                    //Insert Logs
                    $this->insertLogs($session->get('id'),
                        $datetime->format("Y-m-d H:i:s"),
                        'Added table '.$_POST['code'].' named '.$_POST['tableName']);
                }
            }else{
                $model = $em->getRepository('AcmeAdminBundle:Room')->findOneBy(array('id'=>$_POST['id']));
                $msg = $this->translateMessage('LBL_SETTING_UPDATED_SUCCESS');

                //Insert Logs
                $this->insertLogs($session->get('id'),
                    $datetime->format("Y-m-d H:i:s"),
                    'Updated table '.$_POST['code'].' named '.$_POST['tableName']);
            }
            $sVip = isset($_POST['isVIP'])? 1:0;
            
            $model->setCode($_POST['code']);
            $model->setName($_POST['tableName']);
            $model->setLabel($_POST['hallName']);
            $model->setVip($sVip);
            $model->setBetMin($_POST['betMin']);
            $model->setBetMax($_POST['betMax']);
            $model->setTieMin($_POST['tieMin']);
            $model->setTieMax($_POST['tieMax']);
            $model->setPairMin($_POST['pairMin']);
            $model->setPairMax($_POST['pairMax']);
            
            if(isset($_POST['new'])){ 
                $m_model = new Announcement();
                $m_model->setRoomId($_POST['id']);
                $m_model->setMessage($_POST['message']);
            }else{
                foreach( $msgId as $key => $n ) {
                    $m_model = $em->getRepository('AcmeAdminBundle:Announcement')->findOneBy(array('roomId'=>$_POST['id'],'id'=>$n));
                    $m_model->setRoomId($_POST['id']);
                    $m_model->setMessage($msgContent[$key]);
                    $em->persist($m_model);
                }
            }
            
           
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                //Call table-setting API after committing to db
                if($_POST['shoe']==''){
                    $url =  $this->container->getParameter('default_shoe_server_api');
                }else{
                    $url = $_POST['shoe'];
                }
                
                $mobile = $this->getVideoUrlByRoomId($_POST['id'],1);
                $tablet = $this->getVideoUrlByRoomId($_POST['id'],2);
                $desktop = $this->getVideoUrlByRoomId($_POST['id'],3);

                $api = $this->getAPIServer();

                $fields = array(
                        'ROOMID' => urlencode($_POST['id']),
                        'code' => urlencode($_POST['code']),
                        'APISERVER' => $api['url'],
                        'label' => $_POST['hallName'],
                        'name' => $_POST['tableName'],
                        'vip' => urlencode($sVip),
                        'betMin' => urlencode($_POST['betMin']),   
                        'betMax' => urlencode($_POST['betMax']),
                        'tieMin' => urlencode($_POST['tieMin']),
                        'tieMax' => urlencode($_POST['tieMax']),
                        'pairMin' => urlencode($_POST['pairMin']),
                        'pairMax' => urlencode($_POST['pairMax']),
                        'mobile'=> json_encode($mobile,JSON_UNESCAPED_SLASHES),
                        'tablet'=> json_encode($tablet,JSON_UNESCAPED_SLASHES),
                        'desktop'=> json_encode($desktop,JSON_UNESCAPED_SLASHES) 
                );
                $fields_string = http_build_query($fields);

                $ch = curl_init();
                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                $result = curl_exec($ch);
                //End API call
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $msg
                );
                
             }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
            }
        }
        return $this->redirect($this->generateUrl('admin_table_setting')); 
    }

    public function testapiAction(){
//        $url =  $this->container->getParameter('table_setting_api');
// 
//        $mobile = $this->getVideoUrlByRoomId(1,1);
//        $tablet = $this->getVideoUrlByRoomId(1,2);
//        $desktop = $this->getVideoUrlByRoomId(1,3);
//        
//        $fields = array(
//                'ROOMID' => urlencode('1'),
//                'code' => urlencode('MB1'),
//                'APISERVER' => 'http://103.227.177.149:8118',
//                'label' => 'Main Hall',
//                'name' => 'BACCARAT 1',
//                'vip' => urlencode(0),
//                'betMin' => urlencode(100),   
//                'betMax' => urlencode(1000),
//                'tieMin' => urlencode(100),
//                'tieMax' => urlencode(1000),
//                'pairMin' => urlencode(501),
//                'pairMax' => urlencode(65001),
//                'mobile'=> json_encode($mobile,JSON_UNESCAPED_SLASHES),
//                'tablet'=> json_encode($tablet,JSON_UNESCAPED_SLASHES),
//                'desktop'=> json_encode($desktop,JSON_UNESCAPED_SLASHES) 
//                
//        );
//        
//       //$sample =json_encode('[http://120.29.88.116:1935/live/AXIS1.stream_370p/playlist.m3u8,http://120.29.88.116:1935/live/AXIS1.stream_360p/playlist.m3u8]');
//        print_r(json_encode($mobile,JSON_UNESCAPED_SLASHES));
////        print_r(json_encode($tablet));
////        print_r(json_encode($desktop));
//        //print_r(array(json_encode($mobile)));
//        
//        $fields_string = http_build_query($fields);
//        
//        $ch = curl_init();
//
//        //set the url, number of POST vars, POST data
//        curl_setopt($ch,CURLOPT_URL, $url);
//        curl_setopt($ch,CURLOPT_POST, count($fields));
//        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
//
//        $result = curl_exec($ch);
//        print_r($result);
        $room = $this->getRoomDetailsByRoomId(1);
        print_r($room);

        exit();
    }
    
    
    public function addTableAction(){
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') == '' && $isActive==0){
            return new Response("session expired");
        }
        
        $code = $_POST['code'];
        $tableName = $_POST['tableName'];
        $msgId = $_POST['msgId'];
        $msgContent = $_POST['msg'];
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $date = $datetime->format("Y-m-d H:i:s");
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        
        if(isset($_POST['new'])){ 
            $a = $this->getTableSettingCodeAndName($code,$tableName);
              if($a > 0){
                  $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->translateMessage('LBL_SETTING_CODE_NAME_EXIST')    
                );
                  return $this->redirect($this->generateUrl('admin_table_setting_add')); 
              }
              else{
                $model = new Room();
              }

                //Insert Logs
                $this->insertLogs($session->get('id'),
                    $datetime->format("Y-m-d H:i:s"),
                    'Added table '.$_POST['code'].' named '.$_POST['tableName']);
                
            }
            $sVip = isset($_POST['isVIP'])? 1:0;
            
            if ($sVip == '1' ){
                
                $roomType = '2';
            }
            elseif ($_POST['hallName'] == 'Main Hall') {
                
                $roomType = '1';
            }
            elseif ($_POST['hallName'] == 'International Hall') {
                
                $roomType = '3';
            }
            
            $model->setCode($_POST['code']);
            $model->setName($_POST['tableName']);
            $model->setLabel($_POST['hallName']);
            $model->setVip($sVip);
            $model->setBetMin($_POST['betMin']);
            $model->setBetMax($_POST['betMax']);
            $model->setTieMin($_POST['tieMin']);
            $model->setTieMax($_POST['tieMax']);
            $model->setPairMin($_POST['pairMin']);
            $model->setPairMax($_POST['pairMax']);
            $model->setDate($date);
            $model->setRoomType($roomType);
            
            $em->persist($model);
            $em->flush();
            $last_id = $model->getId();

            if(isset($_POST['new'])){ 
                foreach( $msgId as $key => $n ) {
                    $m_model = new Announcement();
                    $m_model->setRoomId($last_id);
                    $m_model->setMessage($msgContent[$key]);
                    $m_model->setLanguageId($key+1);
                    $em->persist($m_model);
                    $em->flush();
                }
            }
            
            $em->getConnection()->commit();
            $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->translateMessage('LBL_SETTING_ADDED_SUCCESS')
                );
        return $this->redirect($this->generateUrl('admin_table_setting'));
    }

    public function deleteTableSettingAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));

        if($session->get('email') == ''){
            return new Response("session expired");
        }

        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }

        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare("DELETE FROM room
          WHERE id=".$_POST['id']."");
        $statement->execute();

        $statement1 = $connection->prepare("DELETE FROM announcement
            WHERE roomId=".$_POST['id']."");
        $statement1->execute();

        //Insert Logs
        $this->insertLogs($session->get('id'),
            $datetime->format("Y-m-d H:i:s"),
            'Deleted a Table');

//        $this->get('session')->getFlashBag()->add(
//                'success',
//              $this->translateMessage('LBL_USERS_DELETE_SUCCESS')
//            );

        return $this->redirect($this->generateUrl('admin_table_setting'));
    }
    
}
        
