<?php

namespace Acme\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\AdminBundle\Model\GlobalModel;
use Acme\AdminBundle\Controller\GlobalController;

class FrontTranslationController extends GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "translation");
        $session->set("url", $this->generateUrl("admin_front_translation"));

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $lang = 'en';
        if(isset($_POST['sel_lang'])){
            $lang = $_POST['sel_lang'];
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){  
        
            if($this->connectMongo()){
                $dbname = $this->connectMongo()->selectDB('ce-pb');
            }else{
                return $this->redirect($this->generateUrl('admin_front_translation')); 
            }
            $collection = $dbname->selectCollection($lang);
     
            $obj = $collection->find();
            $trans = '';
            $bol = false;
            $delete = $this->translateMessage("LBL_FRONT_DELETE_TRANSLATION");
            foreach ($obj as $row){
                    foreach ($row as $item=>$a){
                        if($bol == true){
                            $trans.='<tr><td>'. $item .'</td><td>'. $a.'</td><td><div style="width:150px;"><button class="btn btn-sm btn-primary edit-trans" data-lang="'.$lang.'" data-code="'.$item.'" data-value="'.$a.'">'.$this->translateMessage('LBL_FRONT_EDIT_TRANSLATION').'</button> <button class="btn btn-sm btn-danger delete-trans" data-lang="'.$lang.'" data-code="'.$item.'" data-value="'.$a.'" disabled>'.$delete.'</button></div></td></tr>';
                        }
                        $bol = true;
                    }
            }
            return $this->render('AcmeAdminBundle:FrontTranslation:index.html.twig',array('data'=>$trans,'lang'=>$lang));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout')); 
        }
    }
    
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set("page_id", "translation");

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");


        $lang = 'en';

        switch ($_POST['lan']) {
            case 'en':
                $lang = 'English';
                break;
            case 'cn':
                $lang = 'Chinese';
                break;
            case 'kr':
                $lang = 'Korean';
                break;
            case 'jp':
                $lang = 'Japanese';
                break;
        }
        $isActive = $this->checkUserStatus($session->get('id'));
        $session->set("url", $this->generateUrl("admin_front_translation"));
        if($session->get('email') != '' && $isActive==1){

            return $this->render('AcmeAdminBundle:FrontTranslation:edit.html.twig',array('lan'=>$_POST['lan'],'lang'=>$lang,'code'=>$_POST['lan_code'],'value'=>$_POST['lan_value']));
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
    }
    
    
    public function updateLanguageAction(){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        if($session->get('email') != '' && $isActive==1){  
            if($_POST['lan']){
            $lang = $_POST['lan'];
            
            switch ($lang){
                case 'en':
                    $mongoId = $this->container->getParameter('en');
                    break;
                case 'cn':
                    $mongoId = new \MongoId( $this->container->getParameter('cn'));
                    break;
                case 'kr':
                    $mongoId = new \MongoId( $this->container->getParameter('kr'));
                    break;
                case 'jp':
                    $mongoId = new \MongoId( $this->container->getParameter('jp'));
                    break;
                case 'krs':
                    $mongoId = new \MongoId( $this->container->getParameter('krs'));
                    break;
            }
           
            $dbname = $this->connectMongo()->selectDB('ce-pb');

            $collection = $dbname->selectCollection($lang);

            $save = $collection->update(array('_id' => $mongoId),
                        array(
                            '$set' => array($_POST['code'] => $_POST['translation']),
                        )
                    );

            $api = $this->getAPIServer();
            $url = $api['front_trans_api'];
            $fields = array(
                    'language' => urlencode($lang),
            );
            $fields_string='';
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');
    
            $ch = curl_init();
    
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    
            $result = curl_exec($ch);
    
            //close connection
            curl_close($ch);

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Updated '.$lang.' translation: <br/> Code : '.$_POST['code'].'<br/> Translation : '.$_POST['translation']);

                if($lang == 'en'){
                    $lang1 = 'English Transaltion';
                }
                elseif ($lang == 'cn'){
                    $lang1 = 'Chinese Transaltion';
                }
                elseif ($lang == 'kr'){
                    $lang1 = 'Korean Transaltion';
                }
                elseif ($lang == 'jp'){
                    $lang1 = 'Japanese Transaltion';
                }

            $this->get('session')->getFlashBag()->add(
                'success',
                $lang1.' '.$this->translateMessage("LBL_FRONT_SUCCESS_MESSAGE")
                
            );
            return $this->redirect($this->generateUrl('admin_front_translation')); 
            }
        }
        
    }
    
    public function deleteLanguageAction(){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $session = $this->getRequest()->getSession();
        $isActive = $this->checkUserStatus($session->get('id'));
        
        if($session->get('email') != '' && $isActive==1){  
            if($_POST['lan']){
            $lang = $_POST['lan'];
            
            switch ($lang){
                case 'en':
                    $mongoId = $this->container->getParameter('en');
                    break;
                case 'cn':
                    $mongoId = new \MongoId( $this->container->getParameter('cn'));
                    break;
                case 'kr':
                    $mongoId = new \MongoId( $this->container->getParameter('kr'));
                    break;
                case 'jp':
                    $mongoId = new \MongoId( $this->container->getParameter('jp'));
                    break;
                case 'krs':
                    $mongoId = new \MongoId( $this->container->getParameter('krs'));
                    break;
            }
           
            $dbname = $this->connectMongo()->selectDB('ce-pb');

            $collection = $dbname->selectCollection($lang);

            $save = $collection->update(array('_id' => $mongoId),
                        array(
                            '$unset' => array($_POST['code'] => 1),
                        )
                    );

            $api = $this->getAPIServer();
            $url = $api['front_trans_api'];
            $fields = array(
                    'language' => urlencode($lang),
            );
            $fields_string='';
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');
    
            $ch = curl_init();
    
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    
            $result = curl_exec($ch);
    
            //close connection
            curl_close($ch);

            //Insert Logs
            $this->insertLogs($session->get('id'),
                $datetime->format("Y-m-d H:i:s"),
                'Deleted '.$lang.' translation: <br/> Code : '.$_POST[''].'<br/> Translation : '.$_POST['translation']);

            $this->get('session')->getFlashBag()->add(
            'success',
            $_POST['code'].' for '.$lang.' translation has been deleted successfully.'
                
            );
            return $this->redirect($this->generateUrl('admin_front_translation')); 
            }
        }else{
            return $this->redirect($this->generateUrl('admin_login_account_logout'));
        }
        
    }
}
