<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Json
 *
 * @author Spare1
 */
namespace Acme\AdminBundle\Model;

class Json {
    //put your code here
    /*
     {"sEcho":1
,"iTotalRecords":11
,"iTotalDisplayRecords":10
,"aaData":[
                   ["1","MDH","MDS - API"],["2","P2PS","P2PSClient"]
                  ,["3","RMDS","teswt"],["4","RWDS","test"]
                  ,["4","RWDS","test"],["4","RWDS","test"]
                  ,["4","RWDS","test"],["4","RWDS","test"]
                  ,["4","RWDS","test"],["4","RWDS","test"]
                  ,["4","RWDS","test"]
]}
     */
    public static function jsonDataTable($dataTable,$total,$fieldNames)
    {
            $cells = "";		
            $id = 1;
            foreach($dataTable as $row)
            {
                    $fieldNamesCount=count($fieldNames);

                    $fieldNumber=0;
                    $cells .= '["';

                    for ($i =0 ; $i < $fieldNamesCount; $i++)
                    {

                            $cells .= str_replace('"','\"',$row[$fieldNames[$i]]);

                            if ($i != $fieldNamesCount - 1)

                            $cells .=  '","';
                    }

                    $cells .= '"],';
                    $id ++;
            }

            $cells = rtrim($cells,",");
            $json_data = '{' .

                    '"sEcho": 1,' .

                    '"iTotalRecords": ' . $total . ',' .

                    '"iTotalDisplayRecords": '.$total.',' .

                    '"aaData" : ['.

                    $cells. ']' .

                    '}';

            return $json_data ;
    }
    
    public static function jsonTagsInput($dataTable,$fieldNames)
    {
            $cells = "";		
            $id = 1;
            foreach($dataTable as $row)
            {
                    $fieldNamesCount=count($fieldNames);

                    $fieldNumber=0;
                    $cells .= '"';

                    for ($i =0 ; $i < $fieldNamesCount; $i++)
                    {

                            //$cells .= str_replace('"','\"',$row[$fieldNames[$i]]);
                            $cells .= $row[$fieldNames[$i]];

                            if ($i != $fieldNamesCount - 1)

                            $cells .=  '","';
                    }

                    $cells .= '",';
                    $id ++;
            }

            $cells = rtrim($cells,",");
            $json_data = '['.$cells. ']';

            return $json_data ;
    }
    
}

?>
