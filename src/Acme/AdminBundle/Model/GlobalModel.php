<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalModel
 *
 * @author Allan
 */
namespace Acme\AdminBundle\Model;
class GlobalModel {
    //put your code here
    public function getNumbers($string){
        return preg_replace("/[^0-9]/","",$string);
    }
    
   
    /**
     * @todo check request number
     * @author Allan
     * @param type $request_no
     * @return boolean
     */
    public function isRequestNoValid($request_no){
        if (strpos($request_no,' ') !== false) {
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    /**
     * @todo create directory
     * @author Allan
     * @param type $container_path
     * @param type $propose_dir_name
     */
    public function createDirectory($container_path, $propose_dir_name){
        $dir = $container_path.$propose_dir_name;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
    
    /**
     * @todo generateJsonFile
     * @author allan
     * @param type $file
     * @param type $data
     */
    public function generateJsonFile($file, $data){
        $fp = fopen($file, 'w');
        fwrite($fp, $data);
        fclose($fp);
    }
    /**
    * @todo password generator
    * @author allan
    * @param string $password
    */
    
   public function passGenerator($password){
           return md5(md5($password));
   }
   
   /**
    * @todo disable special characters
    * @param unknown_type $mystring
    */
   public function isSpecialCharPresent($mystring){
           if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '#') === false
                           && strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false
                           && strpos($mystring, '*') === false && strpos($mystring, '_') === false
                           && strpos($mystring, '=') === false && strpos($mystring, '`') === false
                           && strpos($mystring, '"') === false
                           && strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
                           && strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){

                   return FALSE; // special character not present
           }else{
                   return TRUE; // special character present
           }
   }
   
   public function isPhoneNumber($number){
       if( !preg_match('/^(NA|[ 0-9+-]+)$/', $number) ) { 
            return FALSE;
        }else{
            return TRUE;
        }
   }
   /**
    * @todo email validator
    * @author allan
    * @param string $email
    * @return boolean
    */
   public function isEmailValid($email){
           if(!preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i', strtolower($email))){
                   return FALSE;
           }else{
                   return TRUE;
           }
   }
   public function checkDate($mydate) {
        if(substr_count($mydate, '-') == 2){
                list($yy,$mm,$dd)=explode("-",$mydate);
                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                        return checkdate($mm,$dd,$yy);
                }else{
                        return false;
                }
        }else{
                return false;
        }
    }
   public function checkDateTime($mydatetime) {
        if(substr_count($mydatetime, '-') == 2){
                list($date, $time) = explode(" ",$mydatetime);
                list($yy,$mm,$dd)=explode("-",$date);
                list($hh,$mn,$sc)=explode(":",$time);

                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                    return checkdate($mm,$dd,$yy);
                }else{
                    return false;
                }

                if (!is_numeric($hh) && !is_numeric($mn) && !is_numeric($sc)){
                    return false;
                }
        }else{
            return false;
        }
    }
    
    public function isPetTypeValid($type){
        if($type == 'dog' || $type == 'cat' || $type== 'other'){
            return true;
        }else{
            return false;
        }
    }
    
    public function isReportTypeValid($type){
        if(is_numeric($type)){
            if($type == 1 || $type== 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function isPetGenderValid($gender){
        if($gender == 'male' || $gender == 'female' || $gender == 'unknown'){
            return true;
        }else{
            return false;
        }
    }
    
    public function createImageThumb($filename,$modheight,$orig_dir,$copy_dir){
        $file= $orig_dir."/".$filename;
        $path_parts = pathinfo($file);
        
        
        // This sets it to a .jpg, but you can change this to png or gif 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            header('Content-type: image/jpeg'); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            header('Content-type: image/png'); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            header('Content-type: image/gif'); 
        }
        // Setting the resize parameters
        list($width, $height) = getimagesize($file); 
        //$modwidth = 146; 
        $modwidth = $modheight / ($height / $width);
        
        // Creating the Canvas 
        $tn= imagecreatetruecolor($modwidth, $modheight); 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            $source = imagecreatefromjpeg($file); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            $source = imagecreatefrompng($file); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            $source = imagecreatefromgif($file);
        }

        // Resizing our image to fit the canvas 
        //imagecopyresized($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 
        imagecopyresampled($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 


        // Outputs a jpg image, you could change this to gif or png if needed 
        if($path_parts['extension']== 'jpeg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".jpeg");
        }else if($path_parts['extension']== 'JPEG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".JPEG");
        }else if($path_parts['extension']== 'jpg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".jpg");
        }else if($path_parts['extension']== 'JPG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".JPG");
        }else if($path_parts['extension']== 'png'){
            imagepng($tn,$copy_dir."/".$path_parts['filename'].".png");
        }else if($path_parts['extension']== 'PNG'){
            imagepng($tn,$copy_dir."/".$path_parts['filename'].".PNG");
        }else if($path_parts['extension']== 'gif'){
            imagegif($tn,$copy_dir."/".$path_parts['filename'].".gif");
        }else if($path_parts['extension']== 'GIF'){
            imagegif($tn,$copy_dir."/".$path_parts['filename'].".GIF");
        }
    }
    
    public function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return false;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    
    public function getSubdomain($fulldomain){
        $arr_fulldomain= explode(".",$fulldomain);
        $arr_domain = array_slice($arr_fulldomain,-2);
        $domain = implode(".", $arr_domain);
        $subdomain = str_replace($domain, "", $fulldomain);
        $subdomain = trim($subdomain,".");
        $subdomain = trim($subdomain);
        return $subdomain;
    }
    
    function getOS($agent) { 

        $user_agent = $agent;

        $os_platform    =   "Unknown OS Platform";

        $os_array       =   array(
                '/windows nt 10.0/i'    =>  'Windows 10',
                '/windows nt 6.2/i'     =>  'Windows 8',
                '/windows nt 6.1/i'     =>  'Windows 7',
                '/windows nt 6.0/i'     =>  'Windows Vista',
                '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                '/windows nt 5.1/i'     =>  'Windows XP',
                '/windows xp/i'         =>  'Windows XP',
                '/windows nt 5.0/i'     =>  'Windows 2000',
                '/windows me/i'         =>  'Windows ME',
                '/win98/i'              =>  'Windows 98',
                '/win95/i'              =>  'Windows 95',
                '/win16/i'              =>  'Windows 3.11',
                '/macintosh|mac os x/i' =>  'Mac OS X',
                '/mac_powerpc/i'        =>  'Mac OS 9',
                '/linux/i'              =>  'Linux',
                '/ubuntu/i'             =>  'Ubuntu',
                '/iphone/i'             =>  'iPhone',
                '/ipod/i'               =>  'iPod',
                '/ipad/i'               =>  'iPad',
                '/android/i'            =>  'Android',
                '/blackberry/i'         =>  'BlackBerry',
                '/webos/i'              =>  'Mobile'
                            );

        foreach ($os_array as $regex => $value) { 

            if (preg_match($regex, $user_agent)) {
                $os_platform    =   $value;
            }

        }   

        return $os_platform;

    }

    function getBrowser($agent) {

        $user_agent = $agent;
//        print_r($user_agent);
//        $browser = get_browser($user_agent, true);
//        print_r($browser);
//        exit();
        $browser        =   "Unknown Browser";

        $browser_array  =   array(
                '/msie/i'       =>  'Internet Explorer',
                '/firefox/i'    =>  'Firefox',
                '/safari/i'     =>  'Safari',
                '/chrome/i'     =>  'Chrome',
                '/opera/i'      =>  'Opera',
                '/netscape/i'   =>  'Netscape',
                '/maxthon/i'    =>  'Maxthon',
                '/konqueror/i'  =>  'Konqueror',
                '/mobile safari/i'     =>  'Safari',
                '/mobile/i'     =>  'Handheld Browser'
                            );

        foreach ($browser_array as $regex => $value) { 

            if (preg_match($regex, $user_agent)) {
                $browser    =   $value;
            }

        }

        return $browser;

    }
    
}

?>
