<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author leokarl
 */
namespace QuotePacket\FrontBundle\Model;

class Login {
    //put your code here
    
    public function checkLoginInfo($username, $password){
        $global_model = new GlobalModel;
        $error_counter=0; // initialize counter
        $error_details= array();
        
        // username string validation
        if(trim($username) == ''){
            $error_counter += 1;
            $error_details = array('title'=>'Incorrect Email', 'msg'=>'Please enter your valid email address.');
//        }elseif($global_model->isSpecialCharPresent($username)){
//            $error_counter += 1;
//            $error_details = array('title'=>'Incorrect Email', 'msg'=>'Please enter your email address correctly.');
        }elseif(!$global_model->isEmailValid($username)){
            $error_counter += 1;
            $error_details = array('title'=>'Incorrect Email', 'msg'=>'Please enter your email address correctly.');
        }elseif(trim($password) == ''){
            $error_counter += 1;
            $error_details = array('title'=>'Incorrect Password', 'msg'=>'Passwords are case sensitive. Please check your CAPS lock key.');
        }
        
        return array('error_count'=>$error_counter, 'error_detail'=>$error_details);
        
    }
    
    
}

?>
