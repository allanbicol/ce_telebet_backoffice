<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuditTrail
 *
 * @ORM\Table(name="logs")
 * @ORM\Entity
 */
class AuditTrail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="operatedby", type="string", length=255)
     */
    private $operatedby;

    /**
     * @var string
     *
     * @ORM\Column(name="operationtime", type="string", length=255)
     */
    private $operationtime;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string", length=255)
     */
    private $details;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operatedby
     *
     * @param string $operatedby
     * @return AuditTrail
     */
    public function setOperatedby($operatedby)
    {
        $this->operatedby = $operatedby;
    
        return $this;
    }

    /**
     * Get operatedby
     *
     * @return string 
     */
    public function getOperatedby()
    {
        return $this->operatedby;
    }

    /**
     * Set operationtime
     *
     * @param string $operationtime
     * @return AuditTrail
     */
    public function setOperationtime($operationtime)
    {
        $this->operationtime = $operationtime;
    
        return $this;
    }

    /**
     * Get operationtime
     *
     * @return string 
     */
    public function getOperationtime()
    {
        return $this->operationtime;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return AuditTrail
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }
}
