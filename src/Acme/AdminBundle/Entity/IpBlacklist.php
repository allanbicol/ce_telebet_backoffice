<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IpBlacklist
 *
 * @ORM\Table(name="blacklistIP")
 * @ORM\Entity
 */
class IpBlacklist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ipfrom", type="string", length=255)
     */
    private $ipfrom;

    /**
     * @var string
     *
     * @ORM\Column(name="ipto", type="string", length=255)
     */
    private $ipto;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipfrom
     *
     * @param string $ipfrom
     * @return IpBlacklist
     */
    public function setIpfrom($ipfrom)
    {
        $this->ipfrom = $ipfrom;
    
        return $this;
    }

    /**
     * Get ipfrom
     *
     * @return string 
     */
    public function getIpfrom()
    {
        return $this->ipfrom;
    }

    /**
     * Set ipto
     *
     * @param string $ipto
     * @return IpBlacklist
     */
    public function setIpto($ipto)
    {
        $this->ipto = $ipto;
    
        return $this;
    }

    /**
     * Get ipto
     *
     * @return string 
     */
    public function getIpto()
    {
        return $this->ipto;
    }
}
