<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IOSLoginHistory
 *
 * @ORM\Table(name="iosLoginHistory")
 * @ORM\Entity
 */
class IOSLoginHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="deviceType", type="string", length=255)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="dateLogin", type="string", length=255)
     */
    private $dateLogin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255)
     */
    private $ip;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deviceType
     *
     * @param string $deviceType
     * @return IOSLoginHistory
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
    
        return $this;
    }

    /**
     * Get deviceType
     *
     * @return string 
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return IOSLoginHistory
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    
        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set dateLogin
     *
     * @param string $dateLogin
     * @return IOSLoginHistory
     */
    public function setDateLogin($dateLogin)
    {
        $this->dateLogin = $dateLogin;
    
        return $this;
    }

    /**
     * Get dateLogin
     *
     * @return string 
     */
    public function getDateLogin()
    {
        return $this->dateLogin;
    }
    
    /**
     * Set ip
     *
     * @param string $ip
     * @return IOSLoginHistory
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }
}
