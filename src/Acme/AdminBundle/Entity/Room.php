<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Room
 *
 * @ORM\Table(name="room")
 * @ORM\Entity
 */
class Room
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="roomType", type="integer")
     */
    private $roomType;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="vip", type="integer")
     */
    private $vip;

    /**
     * @var string
     *
     * @ORM\Column(name="cameraIp", type="string", length=255)
     */
    private $cameraIp;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="betMin", type="float")
     */
    private $betMin;

    /**
     * @var float
     *
     * @ORM\Column(name="betMax", type="float")
     */
    private $betMax;

    /**
     * @var float
     *
     * @ORM\Column(name="tieMin", type="float")
     */
    private $tieMin;

    /**
     * @var float
     *
     * @ORM\Column(name="tieMax", type="float")
     */
    private $tieMax;

    /**
     * @var float
     *
     * @ORM\Column(name="pairMin", type="float")
     */
    private $pairMin;

    /**
     * @var float
     *
     * @ORM\Column(name="pairMax", type="float")
     */
    private $pairMax;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Room
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Room
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Room
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set roomType
     *
     * @param integer $roomType
     * @return Room
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;
    
        return $this;
    }

    /**
     * Get roomType
     *
     * @return integer 
     */
    public function getRoomType()
    {
        return $this->roomType;
    }
    
    /**
     * Set vip
     *
     * @param integer $vip
     * @return Room
     */
    public function setVip($vip)
    {
        $this->vip = $vip;
    
        return $this;
    }

    /**
     * Get vip
     *
     * @return integer 
     */
    public function getVip()
    {
        return $this->vip;
    }

    /**
     * Set cameraIp
     *
     * @param string $cameraIp
     * @return Room
     */
    public function setCameraIp($cameraIp)
    {
        $this->cameraIp = $cameraIp;
    
        return $this;
    }

    /**
     * Get cameraIp
     *
     * @return string 
     */
    public function getCameraIp()
    {
        return $this->cameraIp;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Room
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set betMin
     *
     * @param float $betMin
     * @return Room
     */
    public function setBetMin($betMin)
    {
        $this->betMin = $betMin;
    
        return $this;
    }

    /**
     * Get betMin
     *
     * @return float 
     */
    public function getBetMin()
    {
        return $this->betMin;
    }

    /**
     * Set betMax
     *
     * @param float $betMax
     * @return Room
     */
    public function setBetMax($betMax)
    {
        $this->betMax = $betMax;
    
        return $this;
    }

    /**
     * Get betMax
     *
     * @return float 
     */
    public function getBetMax()
    {
        return $this->betMax;
    }

    /**
     * Set tieMin
     *
     * @param float $tieMin
     * @return Room
     */
    public function setTieMin($tieMin)
    {
        $this->tieMin = $tieMin;
    
        return $this;
    }

    /**
     * Get tieMin
     *
     * @return float 
     */
    public function getTieMin()
    {
        return $this->tieMin;
    }

    /**
     * Set tieMax
     *
     * @param float $tieMax
     * @return Room
     */
    public function setTieMax($tieMax)
    {
        $this->tieMax = $tieMax;
    
        return $this;
    }

    /**
     * Get tieMax
     *
     * @return float 
     */
    public function getTieMax()
    {
        return $this->tieMax;
    }

    /**
     * Set pairMin
     *
     * @param float $pairMin
     * @return Room
     */
    public function setPairMin($pairMin)
    {
        $this->pairMin = $pairMin;
    
        return $this;
    }

    /**
     * Get pairMin
     *
     * @return float 
     */
    public function getPairMin()
    {
        return $this->pairMin;
    }

    /**
     * Set pairMax
     *
     * @param float $pairMax
     * @return Room
     */
    public function setPairMax($pairMax)
    {
        $this->pairMax = $pairMax;
    
        return $this;
    }

    /**
     * Get pairMax
     *
     * @return float 
     */
    public function getPairMax()
    {
        return $this->pairMax;
    }
}
