<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="roomId", type="integer")
     */
    private $roomId;

    /**
     * @var integer
     *
     * @ORM\Column(name="deviceTypeId", type="integer")
     */
    private $deviceTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="videoEncoder", type="string", length=255)
     */
    private $videoEncoder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set roomId
     *
     * @param integer $roomId
     * @return Video
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;
    
        return $this;
    }

    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set deviceTypeId
     *
     * @param integer $deviceTypeId
     * @return Video
     */
    public function setDeviceTypeId($deviceTypeId)
    {
        $this->deviceTypeId = $deviceTypeId;
    
        return $this;
    }

    /**
     * Get deviceTypeId
     *
     * @return integer 
     */
    public function getDeviceTypeId()
    {
        return $this->deviceTypeId;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Video
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Set videoEncoder
     *
     * @param string $videoEncoder
     * @return Video
     */
    public function setVideoEncoder($videoEncoder)
    {
        $this->videoEncoder = $videoEncoder;
    
        return $this;
    }

    /**
     * Get videoEncoder
     *
     * @return string 
     */
    public function getVideoEncoder()
    {
        return $this->videoEncoder;
    }
}
