<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Server
 *
 * @ORM\Table(name="server")
 * @ORM\Entity
 */
class Server
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="pass_gen_api", type="string", length=255)
     */
    private $pass_gen_api;


    /**
     * @var string
     *
     * @ORM\Column(name="front_trans_api", type="string", length=255)
     */
    private $front_trans_api;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Server
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Set pass_gen_api
     *
     * @param string $pass_gen_api
     * @return Server
     */
    public function setPassGenApi($pass_gen_api)
    {
        $this->pass_gen_api = $pass_gen_api;

        return $this;
    }

    /**
     * Set front_trans_api
     *
     * @param string $front_trans_api
     * @return Server
     */
    public function setFrontTransApi($front_trans_api)
    {
        $this->front_trans_api = $front_trans_api;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get pass_gen_api
     *
     * @return string
     */
    public function getPassGenApi()
    {
        return $this->pass_gen_api;
    }

    /**
     * Get front_trans_api
     *
     * @return string
     */
    public function getFrontTransApi()
    {
        return $this->front_trans_api;
    }
}
