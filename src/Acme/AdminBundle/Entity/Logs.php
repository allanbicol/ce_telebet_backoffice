<?php

namespace Acme\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="logs")
 * @ORM\Entity
 */
class Logs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatedby", type="integer")
     */
    private $operatedby;

    /**
     * @var string
     *
     * @ORM\Column(name="operationtime", type="string")
     */
    private $operationtime;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string")
     */
    private $details;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operatedby
     *
     * @param integer $operatedby
     * @return Logs
     */
    public function setOperatedby($operatedby)
    {
        $this->operatedby = $operatedby;
    
        return $this;
    }

    /**
     * Get operatedby
     *
     * @return integer 
     */
    public function getOperatedby()
    {
        return $this->operatedby;
    }

    /**
     * Set operationtime
     *
     * @param string $operationtime
     * @return Logs
     */
    public function setOperationtime($operationtime)
    {
        $this->operationtime = $operationtime;
    
        return $this;
    }

    /**
     * Get operationtime
     *
     * @return string 
     */
    public function getOperationtime()
    {
        return $this->operationtime;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Logs
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }
}
