<?php

namespace Acme\AdminBundle;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\AdminBundle\Controller\GlobalController;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('translate', array($this, 'translationFilter')),
        );
    }

    public function translationFilter($txt)
    {   

//    $servername = "127.0.0.1";
//    $username = "root";
//    $password = "";
//    $dbname = "codedb";
//    $port = "3306";
    $servername = "103.227.177.149";
    $username = "root";
    $password = "123456";
    $dbname = "codedb";
    $port = "3306";

    $mysqli = new \mysqli($servername, $username, $password,$dbname,$port);
//    if (!$link = mysql_connect($servername, $username, $password)) {
//        echo 'Could not connect to mysql';   
//        exit;
//    }
    if($mysqli->connect_errno){
        echo 'Could not connect to mysql';   
        exit;
    }

//    if (!mysql_select_db($dbname, $link)) {
//        echo 'Could not select database';
//        exit;
//    }
    mysqli_query($mysqli,"SET NAMES 'utf8'");
    if(isset($_COOKIE['lan'])){
        $lang = $_COOKIE['lan'];
    }else{
        setcookie("lan", 'cn');
        $lang = 'cn';
    }

    $sql    = "SELECT translation FROM adminSourceTranslation ast LEFT JOIN adminTranslation adt ON ast.id = adt.id WHERE message='".$txt."' AND language='".$lang."'";
    
    $result = mysqli_query($mysqli,$sql);

    if (!$result) {
        echo "DB Error, could not query the database\n";
        echo 'MySQL Error: ' . mysqli_connect_error();
        exit;
    }

    $data = '';
    while ($row = mysqli_fetch_array($result)) {
        $data =  $row["translation"];
    }

        return $data;
    }

    public function getName()
    {
        return 'app_extension';
    }
    
}
?>
