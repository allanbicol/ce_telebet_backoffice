$(document).ready(function() {
  var handleDataTableButtons = function() {
    if ($("#datatable").length) {
      $("#datatable").DataTable({
//        dom: "Bfrtip",
        "bSort" : false,
          "language": {
            "lengthMenu": ""+show+" _MENU_ "+entries+"",
            'search': search,
            'paginate': {
            'previous': previous,
            'next' : next
            },
            "info": " "+showing+" _START_ "+to+" _END_ "+of+" _TOTAL_ "+entries+"",
            "zeroRecords": empty_record,
            "infoEmpty": ""+showing+" 0 "+to+" 0 "+of+" 0 "+entries+"",
            "emptyTable": empty
          },

        buttons: [
          {
            extend: "copy",
            className: "btn-sm"
          },
          {
            extend: "csv",
            className: "btn-sm"
          },
          {
            extend: "excel",
            className: "btn-sm"
          },
          {
            extend: "pdfHtml5",
            className: "btn-sm"
          },
          {
            extend: "print",
            className: "btn-sm"
          },
        ],
        responsive: true,
        "columnDefs": [
          {
            "targets": [2],
            "visible": false,
            "searchable": false
          }
        ],
      });
    }
  };

  TableManageButtons = function() {
    "use strict";
    return {
      init: function() {
        handleDataTableButtons();
      }
    };
  }();

  //$('#datatable').dataTable();

  $('#datatable-keytable').DataTable({
    keys: true
  });

  $('#datatable-responsive').DataTable();

  $('#datatable-scroller').DataTable({
    ajax: "js/datatables/json/scroller-demo.json",
    deferRender: true,
    scrollY: 380,
    scrollCollapse: true,
    scroller: true
  });

  $('#datatable-fixed-header').DataTable({
    fixedHeader: true
  });

  var $datatable = $('#datatable-checkbox');

  $datatable.dataTable({
    //'order': [[ 1, 'asc' ]],
    'columnDefs': [
      { orderable: false, targets: [0] }
    ]
  });
  $datatable.on('draw.dt', function() {
    $('input').iCheck({
      checkboxClass: 'icheckbox_flat-green'
    });
  });

  TableManageButtons.init();
});


$(".delete").click(function(){
   var $self = $(this);
    bootbox.dialog({
        message: delete_msg,
        buttons: {
          success: {
            label: delete_ok,
            className: "btn-primary",
            callback: function() {
                var id = $self.attr('data-id');
            $.ajax({
                url: delete_url,
                method: 'POST',
                data: {'id': id},
                success: function(data) {
                    // location.reload(true);
                  $('html, body').animate({ scrollTop: 0 }, 0);
                  $("#alert").html(delete_message_success);
                  $("#alert").removeClass('hidden');
                  setTimeout(function () { // wait 3 seconds and reload
                    window.location.reload(true);
                  }, 3000);
                },
                error:function(){
                }
            });
            }
          }
        }
      });
});