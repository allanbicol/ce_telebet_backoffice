/**
 * Created by allanbicol on 9/16/16.
 */
$(document).ready(function() {
    var ctx = document.getElementById("mybarChart");
//        var d = new Date();
//        d.setDate(d.getDate() - 2);

    var arr = [];
    var s = 0;
    for(i=6;i>=0;i--){
        var s = i;
        var d = new Date();

        d.setDate(d.getDate() - i);

        arr[i]= d.getFullYear()+'-'+ (d.getMonth()+1) +'-'+ d.getDate();
    }

    var mybarChart = new Chart(ctx, {

        type: 'bar',
        data: {
            labels: [arr[6], arr[5], arr[4], arr[3], arr[2], arr[1], arr[0]],
            datasets: [{
                label: iphone_user,
                backgroundColor: "#3498DB",
                data: [day7_iPhone, day6_iPhone, day5_iPhone, day4_iPhone, day3_iPhone, day2_iPhone, day1_iPhone]
            }, {
                label: ipad_user,
                backgroundColor: "#1ABB9C",
                data: [day7_iPad, day6_iPad, day5_iPad, day4_iPad, day3_iPad, day2_iPad, day1_iPad]
            }, {
                label: android_user,
                backgroundColor: "#9B59B6",
                data: [day7_android, day6_android, day5_android, day4_android, day3_android, day2_android , day1_android]
            }
            ]
        },

        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

});