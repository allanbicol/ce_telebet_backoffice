$(document).ready(function() {
var handleDataTableButtons = function() {
      if ($("#datatable").length) {
        $("#datatable").DataTable({
          //dom: "Bfrtip",
          'searching':true,
          'order': [[ 0, 'asc' ]],
          "bSort" : false,
          "language": {
            "lengthMenu": ""+show+" _MENU_ "+entries+"",
            'search': search,
            'paginate': {
            'previous': previous,
            'next' : next
            },
            "info": " "+showing+" _START_ "+to+" _END_ "+of+" _TOTAL_ "+entries+"",
            "zeroRecords": empty_record,
            "infoEmpty": ""+showing+" 0 "+to+" 0 "+of+" 0 "+entries+"",
            "emptyTable": empty
          },
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    
    $('#datatable-responsive').DataTable();

   

    var $datatable = $('#datatable-checkbox');

    $datatable.dataTable({
      //'order': [[ 1, 'asc' ]],
      'columnDefs': [
        { orderable: false, targets: [0] },
        { "width": "200px", "targets": 11 }
      ]
    });
    $datatable.on('draw.dt', function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_flat-green'
      });
    });

    TableManageButtons.init();
    
});

$('.lang-options').click(function(){
    var $self = $(this);
    $('input[name="sel_lang"]').val($self.val());
    $('#frm-sel-lang').submit();
});

$('.edit-trans').click(function(){
    var $self = $(this);
    //alert($self.attr('data-lang'));
    $('input[name="lan"]').val($self.attr('data-lang'));
    $('input[name="lan_code"]').val($self.attr('data-code'));
    $('input[name="lan_value"]').val($self.attr('data-value'));
    $('#frm-edit-trans').submit();
    
});

//$('.delete-trans').click(function(){
//    var $self = $(this);
//    var con = confirm(delete_msg);
//    if (con == true) {
//        $.ajax({
//            url: delete_url,
//            method: 'POST',
//            data: {'lan': $self.attr('data-lang'),'code':$self.attr('data-code'),'translation':$self.attr('data-value')},
//            success: function(data) {
//                location.reload();
//            },
//            error:function(){
//            }
//        });
//    }
//});

$(".delete-trans").click(function(){
   var $self = $(this);
    bootbox.dialog({
        message: delete_msg,
        buttons: {
          success: {
            label: delete_ok,
            className: "btn-primary",
            callback: function() {
                var id = $self.attr('data-id');
            $.ajax({
                url: delete_url,
                method: 'POST',
                data: {'id': id},
                success: function(data) {
                    location.reload(true);
                },
                error:function(){
                }
            });
            }
          }
        }
      });
});