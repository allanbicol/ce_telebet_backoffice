$(document).ready(function() {
  var handleDataTableButtons = function() {
    if ($("#datatable").length) {
      $("#datatable").DataTable({
//        dom: "Bfrtip",
        'order': [[ 0, 'desc' ]],
        "bSort" : false,
          "language": {
            "lengthMenu": ""+show+" _MENU_ "+entries+"",
            'search': search,
            'paginate': {
            'previous': previous,
            'next' : next
            },
            "info": " "+showing+" _START_ "+to+" _END_ "+of+" _TOTAL_ "+entries+"",
            "zeroRecords": empty_record,
            "infoEmpty": ""+showing+" 0 "+to+" 0 "+of+" 0 "+entries+"",
            "emptyTable": empty
          },
        buttons: [
          {
            extend: "copy",
            className: "btn-sm"
          },
          {
            extend: "csv",
            className: "btn-sm"
          },
          {
            extend: "excel",
            className: "btn-sm"
          },
          {
            extend: "pdfHtml5",
            className: "btn-sm"
          },
          {
            extend: "print",
            className: "btn-sm"
          },
        ],
        responsive: true
      });
    }
  };

  TableManageButtons = function() {
    "use strict";
    return {
      init: function() {
        handleDataTableButtons();
      }
    };
  }();

  //$('#datatable').dataTable();

  $('#datatable-keytable').DataTable({
    keys: true
  });

  $('#datatable-responsive').DataTable();

  $('#datatable-scroller').DataTable({
    ajax: "js/datatables/json/scroller-demo.json",
    deferRender: true,
    scrollY: 380,
    scrollCollapse: true,
    scroller: true
  });

  $('#datatable-fixed-header').DataTable({
    fixedHeader: true
  });

  var $datatable = $('#datatable-checkbox');

  $datatable.dataTable({
    //'order': [[ 1, 'asc' ]],
    'columnDefs': [
      { orderable: false, targets: [0] }
    ]
  });
  $datatable.on('draw.dt', function() {
    $('input').iCheck({
      checkboxClass: 'icheckbox_flat-green'
    });
  });

  TableManageButtons.init();
});

$(".btnSubmit").click(function(){
    var date_from = $('#dateFrom').val();
    var date_to = $('#dateTo').val();
    
    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('submit');
    $('#frmFilter').submit();
});
$(".btnToday").click(function(){
    var date = moment();
    $('#dateFrom').val(date.format('YYYY-MM-DD'));
    $('#dateTo').val(date.format('YYYY-MM-DD'));
    var date_from = date.format('YYYY-MM-DD');
    var date_to = date.format('YYYY-MM-DD');

    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('today');
    $('#frmFilter').submit();
});
$(".btnYesterday").click(function(){
    var date = moment().subtract(1, 'day')
    $('#dateFrom').val(date.format('YYYY-MM-DD'));
    $('#dateTo').val(date.format('YYYY-MM-DD'));
    var hall = $('select[name="sl_hall"]').val();
    var table = $('select[name="sl_table"]').val();
    var date_from = $('#dateFrom').val();
    var date_to = $('#dateTo').val();
    
    $('input[name="fltr_hall"]').val(hall);
    $('input[name="fltr_table"]').val(table);
    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('today');
    $('#frmFilter').submit();
});

$(document).ready(function() {
  //getTable('all'); 
  var date = moment();
  
  $('#dateFrom').val(dateFrom);
  $('#dateTo').val(dateTo);
  $('select[name="hourFrom"]').val(hourFrom);
  $('select[name="hourTo"]').val(hourTo);
  
  $('#dateFrom').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_4",
    format:'YYYY-MM-DD'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
  $('#dateTo').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_4",
    format:'YYYY-MM-DD'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });  

});