$(document).ready(function() {
    var handleDataTableButtons = function() {
        if ($("#datatable").length) {
            $("#datatable").DataTable({
                'order': [[ 0, 'asc' ]],
                "bSort" : false,
                "language": {
                    "lengthMenu": ""+show+" _MENU_ "+entries+"",
                    'search': search,
                    'paginate': {
                        'previous': previous,
                        'next' : next
                    },
                    "info": " "+showing+" _START_ "+to+" _END_ "+of+" _TOTAL_ "+entries+"",
                    "zeroRecords": empty_record,
                    "infoEmpty": ""+showing+" 0 "+to+" 0 "+of+" 0 "+entries+"",
                    "emptyTable": empty
                },
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    }
                ],
                responsive: true,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    }

                ]
            });
        }
    };

    TableManageButtons = function() {
        "use strict";
        return {
            init: function() {
                handleDataTableButtons();
            }
        };
    }();


    $('#datatable-responsive').DataTable();



    var $datatable = $('#datatable-checkbox');

    $datatable.dataTable({
        //'order': [[ 1, 'asc' ]],
        'columnDefs': [
            { orderable: false, targets: [0] },
            { "width": "200px", "targets": 11 }
        ]
    });
    $datatable.on('draw.dt', function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });
    });

    TableManageButtons.init();

});


//$('.delete').click(function(){
//    var $self = $(this);
//    var con = confirm(delete_msg);
//    if (con == true) {
//        var id = $self.attr('data-id');
//        $.ajax({
//            url: delete_url,
//            method: 'POST',
//            data: {'id': id},
//            success: function(data) {
//                if(data=='success'){
//                    location.reload();
//                }
//            },
//            error:function(){
//            }
//        });
//    }
//});


$(".delete").click(function(){
    var $self = $(this);
    bootbox.dialog({
        message: delete_msg,
        buttons: {
            success: {
                label: delete_ok,
                className: "btn-primary",
                callback: function() {
                    var id = $self.attr('data-id');
                    $.ajax({
                        url: delete_url,
                        method: 'POST',
                        data: {'id': id},
                        success: function(data) {
                            // location.reload();
                            $('html, body').animate({ scrollTop: 0 }, 0);
                            $("#alert").removeClass('hidden');
                            $(".alert-success").html(delete_message_success);
                            setTimeout(function () { // wait 3 seconds and reload
                                window.location.reload(true);
                            }, 3000);
                        },
                        error:function(){
                        }
                    });
                }
            }
        }
    });
});