$(document).ready(function() {
var handleDataTableButtons = function() {
      if ($("#datatable").length) {
        $("#datatable").DataTable({
          dom: "Bfrtip",
          'pageLength':20,
          'order': [[ 0, 'asc' ]],
          "bSort" : false,
          "language": {
            "lengthMenu": ""+show+" _MENU_ "+entries+"",
            'search': search,
            'paginate': {
            'previous': previous,
            'next' : next
            },
            "info": " "+showing+" _START_ "+to+" _END_ "+of+" _TOTAL_ "+entries+"",
            "zeroRecords": empty_record,
            "infoEmpty": ""+showing+" 0 "+to+" 0 "+of+" 0 "+entries+"",
            "emptyTable": empty
          },
          buttons: [
            {
              extend: "copy",
              text: copy,
              className: "btn-sm"
            },
            {
              extend: "csv",
              text: csv,
              className: "btn-sm"
            },
            {
              extend: "excel",
              text: excel,
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              text: print,
              className: "btn-sm"
            },
          ],
          responsive: true,
           "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                { "sClass": "banker_class", "aTargets": [ 5 ] },
                { "sClass": "player_class", "aTargets": [ 6 ] }

            ]
            
        });
      }
    }; 

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    
    $('#datatable-responsive').DataTable();

   

    var $datatable = $('#datatable-checkbox');

    $datatable.dataTable({
      //'order': [[ 1, 'asc' ]],
      'columnDefs': [
        { orderable: false, targets: [0] },
        { "width": "200px", "targets": 11 }
      ]
    });
    $datatable.on('draw.dt', function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_flat-green'
      });
    });

    TableManageButtons.init();
    
});

$(".btnSubmit").click(function(){
    var hall = $('select[name="sl_hall"]').val();
    var table = $('select[name="sl_table"]').val();
    var shoe = $('select[name="sl_shoe"]').val();
    var date_from = $('#dateFrom').val();
    var date_to = $('#dateTo').val();
    
    $('input[name="fltr_hall"]').val(hall);
    $('input[name="fltr_table"]').val(table);
    $('input[name="shoe"]').val(shoe);
    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('submit');
    $('#frmFilter').submit();
});
$(".btnToday").click(function(){
    var date = moment();
    $('#dateFrom').val(date.format('YYYY-MM-DD'));
    $('#dateTo').val(date.format('YYYY-MM-DD'));
    var hall = $('select[name="sl_hall"]').val();
    var shoe = $('select[name="sl_shoe"]').val();
    var table = $('select[name="sl_table"]').val();
    var date_from = date.format('YYYY-MM-DD');
    var date_to = date.format('YYYY-MM-DD');
    
    $('input[name="fltr_hall"]').val(hall);
    $('input[name="fltr_table"]').val(table);
    $('input[name="shoe"]').val(shoe);
    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('today');
    $('#frmFilter').submit();
});
$(".btnYesterday").click(function(){
    var date = moment().subtract(1, 'day')
    $('#dateFrom').val(date.format('YYYY-MM-DD'));
    $('#dateTo').val(date.format('YYYY-MM-DD'));
    var hall = $('select[name="sl_hall"]').val();
    var table = $('select[name="sl_table"]').val();
    var shoe = $('select[name="sl_shoe"]').val();
    var date_from = $('#dateFrom').val();
    var date_to = $('#dateTo').val();
    
    $('input[name="fltr_hall"]').val(hall);
    $('input[name="fltr_table"]').val(table);
    $('input[name="shoe"]').val(shoe);
    $('input[name="fltr_date_from"]').val(date_from);
    $('input[name="fltr_date_to"]').val(date_to);
    $('input[name="fltr_hour_from"]').val($('select[name="hourFrom"]').val());
    $('input[name="fltr_hour_to"]').val($('select[name="hourTo"]').val());
    $('input[name="btn_function"]').val('today');
    $('#frmFilter').submit();
});

$('#sl_hall').change(function(){
    var $self = $(this);
    //alert($self.val());
    getTable($self.val());
    $('#sl_table').val(table_selected);
});


$(document).ready(function() {
  getTable('all'); 
  var date = moment();
  
  $('#dateFrom').val(dateFrom);
  $('#dateTo').val(dateTo);
  $('select[name="hourFrom"]').val(hourFrom);
  $('select[name="hourTo"]').val(hourTo);
  
  $('#dateFrom').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_4",
    format:'YYYY-MM-DD'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });
  $('#dateTo').daterangepicker({
    singleDatePicker: true,
    calender_style: "picker_4",
    format:'YYYY-MM-DD'
  }, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
  });  

});


function getTable(hall){
    $.ajax({
            url: get_table_url,
            method: 'POST',
            data: {'hall': hall,'tlb_select':table_selected},
            success: function(data) {
                var dataVal = jQuery.parseJSON(data);
                var str = '<select class="form-control" id="sl_table" name="sl_table">';
                if(table_selected=='all'){
                    str += '<option value="all" selected>All</option>';
                }else{
                    str += '<option value="all" >All</option>';
                }
                for ($i=0;$i<dataVal.length;$i++){
                    if(table_selected==dataVal[$i].id){
                        str += '<option value="'+dataVal[$i].id+'" selected>'+dataVal[$i].name+'</option>';
                    }else{
                        str += '<option value="'+dataVal[$i].id+'">'+dataVal[$i].name+'</option>';
                    }
                }
                str += '</select>';
               $('.tbl_select').html(str);
            },
            error:function(){
            }
        });
}